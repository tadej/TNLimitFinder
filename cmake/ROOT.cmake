# Define the ROOT version
set(ROOT_VERSION_DEFAULT 6.22.00)
set(ROOT_VERSION ${ROOT_VERSION_DEFAULT} CACHE STRING "ROOT version required")
message(STATUS "Building with ROOT version ${ROOT_VERSION}")
if(NOT ${ROOT_VERSION_DEFAULT} STREQUAL ${ROOT_VERSION})
    message(WARNING "You are using non-default version of ROOT. Batch submission might fail!")
endif()

# Set ROOT path and try to autodetect it from ENV
set(ROOTSYS $ENV{ROOTSYS} CACHE STRING "ROOT path")

# You need to tell CMake where to find the ROOT installation. This can be done in a number of ways:
#   - ROOT built with classic configure/make use the provided $ROOTSYS/etc/cmake/FindROOT.cmake
#   - ROOT built with CMake. Add in CMAKE_PREFIX_PATH the installation prefix for ROOT
list(APPEND CMAKE_PREFIX_PATH ${ROOTSYS})

#---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
find_package(ROOT ${ROOT_VERSION} EXACT REQUIRED
             COMPONENTS Core RooStats)

#---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
include(${ROOT_USE_FILE})
