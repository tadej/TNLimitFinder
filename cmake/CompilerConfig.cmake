# Use C++17
set(CMAKE_CXX_STANDARD 17)

# Build configuration
if("${CMAKE_BUILD_TYPE}" STREQUAL "")
    set(CMAKE_BUILD_TYPE "RelWithDebInfo")
endif()
message(STATUS "Using build configuration: ${CMAKE_BUILD_TYPE}")

# Various compiler configs
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Werror -Wno-unknown-pragmas")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Werror -Wno-unknown-pragmas")

# Coverage report
set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -coverage")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -coverage")

# Make final release build smaller
if(CMAKE_COMPILER_IS_GNUCXX)
    message(STATUS "Using GCC")
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -Wl,--no-as-needed")
    set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} -s")
endif()

# Set proper install rpath
SET(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")

# Generate compile_commands.json
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)


# clang-tidy
set(CLANG_TIDY OFF CACHE BOOL "Enable clang tidy")
if (CLANG_TIDY)
    find_program(
        CLANG_TIDY_EXE
        NAMES "clang-tidy"
        DOC "Path to clang-tidy executable"
    )
endif()
if(NOT CLANG_TIDY_EXE)
    message(STATUS "clang-tidy disabled or not found")
else()
    message(STATUS "clang-tidy found: ${CLANG_TIDY_EXE}")
    set(CLANG_TIDY_COMMAND "${CLANG_TIDY_EXE}" "--header-filter=src/.*")
endif()
