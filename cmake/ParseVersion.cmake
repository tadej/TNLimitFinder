# Read version from file
FILE(READ VERSION TNLimitFinder_VERSION)
STRING(REGEX REPLACE "\n" "" TNLimitFinder_VERSION "${TNLimitFinder_VERSION}") # get rid of the newline at the end
MESSAGE(STATUS "Building TNLimitFinder ${TNLimitFinder_VERSION}")

# Find Git Version Patch
FIND_PROGRAM(GIT git)
IF(GIT AND NOT NO_GIT)
    EXECUTE_PROCESS(
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        COMMAND ${GIT} rev-parse --short HEAD
        OUTPUT_VARIABLE GIT_OUT OUTPUT_STRIP_TRAILING_WHITESPACE
    )
    IF(NOT GIT_OUT STREQUAL "")
        SET(TNLimitFinder_VERSION_VCS "${GIT_OUT}")
        MESSAGE(STATUS "Git revision: ${GIT_OUT}")
    ENDIF()
ELSEIF(GITBUILD)
    SET(TNLimitFinder_VERSION_VCS "${GITBUILD}")
ELSE()
    SET(GIT_OUT 0)
ENDIF()
