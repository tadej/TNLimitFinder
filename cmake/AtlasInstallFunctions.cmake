# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# This is a generic function for installing practically any type of file
# from a package into both the build and the install areas. Behind the scenes
# it is used by most of the functions of this file.
#
# Based on atlas_install_generic.
#
# Usage: tn_install_generic( dir/file1 dir/dir2...
#                            DESTINATION dir )
#
function( tn_make_symlink )

   # Parse the options given to the function:
   cmake_parse_arguments( ARG "EXECUTABLE" "DESTINATION;" "" ${ARGN} )

   # If there are no file/directory names given to the function, return now:
   if( NOT ARG_UNPARSED_ARGUMENTS )
      message( WARNING "Function received no file/directory arguments" )
      return()
   endif()
   if( NOT ARG_DESTINATION )
      message( WARNING "No destination was specified" )
      return()
   endif()

   set( _tgtName TNInstall_Symlink )
   _atlas_create_install_target( ${_tgtName} )

   # Define what the build and install area destinations should be:
   set( _buildDest ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${ARG_DESTINATION} )
   set( _file ${ARG_UNPARSED_ARGUMENTS} )

   file( RELATIVE_PATH _target
      ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM} ${CMAKE_CURRENT_SOURCE_DIR}/${_file} )
   add_custom_command( OUTPUT ${_buildDest}
      COMMAND ${CMAKE_COMMAND} -E create_symlink ${_target}
      ${_buildDest} )

   set_property( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} APPEND PROPERTY
      ADDITIONAL_MAKE_CLEAN_FILES
      ${_buildDest} )
   # Add it to the installation target:
   set_property( TARGET ${_tgtName} APPEND PROPERTY
      INSTALLED_FILES ${_buildDest} )

endfunction( tn_make_symlink )

# This is a generic function for installing practically any type of file
# from a package into both the build and the install areas. Behind the scenes
# it is used by most of the functions of this file.
#
# Based on atlas_install_generic.
#
# Usage: tn_install_generic( dir/file1 dir/dir2...
#                            DESTINATION dir
#                            [BUILD_DESTINATION dir]
#                            [TYPENAME type]
#                            [EXECUTABLE] )
#
function( tn_install_generic )

   # Parse the options given to the function:
   cmake_parse_arguments( ARG "EXECUTABLE" "TYPENAME;DESTINATION;BUILD_DESTINATION" "" ${ARGN} )

   # If there are no file/directory names given to the function, return now:
   if( NOT ARG_UNPARSED_ARGUMENTS )
      message( WARNING "Function received no file/directory arguments" )
      return()
   endif()
   if( NOT ARG_DESTINATION )
      message( WARNING "No destination was specified" )
      return()
   endif()

   # Create an installation target for this type:
   if( ARG_TYPENAME )
       set( _tgtName TNInstall_${ARG_TYPENAME} )
    else()
       set( _tgtName TNInstall_Generic )
    endif()

   _atlas_create_install_target( ${_tgtName} )

   # Expand possible wildcards:
   file( GLOB_RECURSE _files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
      ${ARG_UNPARSED_ARGUMENTS} )

   # Define what the build and install area destinations should be:
   set( _buildDest ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/${ARG_DESTINATION} )
   set( _installDest ${ARG_DESTINATION} )

   # Now loop over all file names:
   foreach( _file ${_files} )
      # Set up its installation into the build area:
      file( RELATIVE_PATH _target
         ${_buildDest} ${CMAKE_CURRENT_SOURCE_DIR}/${_file} )
      get_filename_component( _filename ${_file} NAME )
      add_custom_command( OUTPUT ${_buildDest}/${_filename}
         COMMAND ${CMAKE_COMMAND} -E make_directory ${_buildDest}
         COMMAND ${CMAKE_COMMAND} -E create_symlink ${_target}
         ${_buildDest}/${_filename} )
      set_property( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} APPEND PROPERTY
         ADDITIONAL_MAKE_CLEAN_FILES
         ${_buildDest}/${_filename} )
      # Add it to the installation target:
      set_property( TARGET ${_tgtName} APPEND PROPERTY
         INSTALLED_FILES ${_buildDest}/${_filename} )
      # Set up its installation into the install area:
      if( IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${_file} )
         install( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${_file}
            DESTINATION ${_installDest}
            USE_SOURCE_PERMISSIONS
            PATTERN ".svn" EXCLUDE )
      else()
         # In case this turns out to be a symbolic link, install the actual
         # file that it points to, using the name of the symlink.
         get_filename_component( _realpath
            ${CMAKE_CURRENT_SOURCE_DIR}/${_file} REALPATH )
         if( ARG_EXECUTABLE )
            install( PROGRAMS ${_realpath}
               DESTINATION ${_installDest}
               RENAME ${_filename} )
         else()
            install( FILES ${_realpath}
               DESTINATION ${_installDest}
               RENAME ${_filename} )
         endif()
      endif()
   endforeach()

endfunction( tn_install_generic )

# This function installs files from the package into the
# right place in both the build and the install directories.
#
# Based on atlas_install_python_modules.
#
# Usage: tn_install_files( someFiles )
#
function( tn_install_files )

   cmake_parse_arguments( ARG "" "" "" ${ARGN} )

   # Call the generic function:
   tn_install_generic( ${ARG_UNPARSED_ARGUMENTS}
      DESTINATION "."
      BUILD_DESTINATION ${CMAKE_OUTPUT_DIRECTORY}
      TYPENAME File )

endfunction( tn_install_files )

# This function installs scripts into the
# right place in both the build and the install directories.
#
# Based on atlas_install_python_modules.
#
# Usage: tn_install_scripts( scripts/*.sh
#                            FOLDER folder )
#
function( tn_install_scripts )

   cmake_parse_arguments( ARG "" "FOLDER" "" ${ARGN} )
   if( NOT ARG_FOLDER )
      message( WARNING "No folder name was specified" )
      return()
   endif()

   # Call the generic function:
   tn_install_generic( ${ARG_UNPARSED_ARGUMENTS}
      DESTINATION ${CMAKE_INSTALL_SCRIPTSDIR}/${ARG_FOLDER}
      BUILD_DESTINATION ${CMAKE_SCRIPTS_OUTPUT_DIRECTORY}/${ARG_FOLDER}
      TYPENAME Scripts )

endfunction( tn_install_scripts )

# This function installs python modules from the package into the
# right place in both the build and the install directories.
#
# Based on atlas_install_python_modules.
#
# Usage: tn_install_python_module( python/SomeDir/*.py
#                                  PACKAGE pkgName )
#
function( tn_install_python_module )
    
   cmake_parse_arguments( ARG "" "PACKAGE" "" ${ARGN} )
   if( NOT ARG_PACKAGE )
      message( WARNING "No package name was specified" )
      return()
   endif()

   # Call the generic function:
   tn_install_generic( ${ARG_UNPARSED_ARGUMENTS}
      DESTINATION ${CMAKE_INSTALL_PYTHONDIR}/${ARG_PACKAGE}
      BUILD_DESTINATION ${CMAKE_PYTHON_OUTPUT_DIRECTORY}/${ARG_PACKAGE}
      TYPENAME Python )
  
   # Byte-compile the python code. To check if it has any syntax errors.

   # Find python:
   find_package( PythonInterp QUIET )
   if( NOT PYTHONINTERP_FOUND )
      # This is very weird, but okay...
      return()
   endif()

   # As a first thing, let's get a list of the files/directories specified:
   file( GLOB_RECURSE _files RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${ARG_UNPARSED_ARGUMENTS} )

   # Create an installation target for the package's python bytecode:
   set( _tgtName TNInstall_PythonBytecodeInstall )
   _atlas_create_install_target( ${_tgtName} )

   # Now loop over all file names:
   foreach( _file ${_files} )

      # Decide if it's a directory, or a simple file:
      if( IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${_file}" )

         # With directories we need to be careful. As we are not allowed to
         # directly write into them. (Modifying the source directory.) Instead
         # the byte-compiled code is not put visibly into the build directory,
         # it is only installed into the "right place".

         # Get the first directory name in this path:
         _atlas_find_first_dir_in_path( ${_file} _firstdir )

         # Get a list of all the python files inside the directory:
         file( GLOB_RECURSE _pyFiles
            RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
            "${_file}/*.py" )

         # Loop over the python file names:
         foreach( _pyFile ${_pyFiles} )

            # Check if the extension is really py
            get_filename_component( _fileext "${_file}" EXT )
            if( NOT ${_fileext} STREQUAL "py" )
               message( STATUS "Ignoring file ${_file} with extension ${_fileext}" )

               unset( _fileext )

               continue()
            endif()

            # Dissect the file name:
            get_filename_component( _path ${_pyFile} PATH )
            string( REPLACE "${_firstdir}/" "" _pathWoFirstdir "${_path}" )
            get_filename_component( _filename ${_pyFile} NAME_WE )

            # The directory to put the bytecode file in:
            set( _bytecodeDir
               "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${ARG_PACKAGE}" )
            set( _bytecodeDir "${_bytecodeDir}/pythonBytecode/${_path}" )

            # Construct the bytecode's file name:
            set( _bytecode "${_bytecodeDir}/${_filename}.pyc" )

            # The python command to execute:
            set( _pycmd "import py_compile; py_compile.compile( " )
            set( _pycmd "${_pycmd}'${CMAKE_CURRENT_SOURCE_DIR}" )
            set( _pycmd "${_pycmd}/${_pyFile}', cfile = '${_bytecode}'" )
            set( _pycmd "${_pycmd}, doraise = True )" )

            # Generate a python file with this content. Unfortunately this is
            # necessary, because in CTEST_USE_LAUNCHERS=TRUE mode the quotes in
            # the custom command would be swallowed.
            string( REGEX REPLACE "(\\.|\\/)" "_" _bytecodeFname
               "${_path}${_filename}" )
            set( _pycmdFile
               "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${ARG_PACKAGE}" )
            set( _pycmdFile "${_pycmdFile}/${_bytecodeFname}pycGen.py" )
            file( GENERATE OUTPUT ${_pycmdFile}
               CONTENT "${_pycmd}" )

            # Set up a custom command for generating the bytecode:
            add_custom_command( OUTPUT ${_bytecode}
               COMMAND ${CMAKE_COMMAND} -E make_directory ${_bytecodeDir}
               COMMAND ${PYTHON_EXECUTABLE} ${_pycmdFile}
               DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/${_pyFile}"
               VERBATIM )

            # Make our installation target call this custom command:
            set_property( TARGET ${_tgtName} APPEND PROPERTY
               INSTALLED_FILES ${_bytecode} )

            # Install this file:
            install( FILES ${_bytecode}
               DESTINATION
               ${CMAKE_INSTALL_PYTHONDIR}/${ARG_PACKAGE}/${_pathWoFirstdir} )

            # Remember to clean out these files when asked:
            set_property( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} APPEND PROPERTY
               ADDITIONAL_MAKE_CLEAN_FILES ${_bytecode} ${_pycmdFile} )

            # Clean up:
            unset( _path )
            unset( _fileext )
            unset( _filename )
            unset( _bytecodeDir )
            unset( _bytecode )
            unset( _pycmd )
            unset( _bytecodeFname )
            unset( _pycmdFile )

         endforeach()

         # Clean up:
         unset( _firstdir )
         unset( _pyFiles )

      else()

         # For files the setup is a little simpler. Here we can just compile
         # the python code "into the right place", and install it from there.

         # Check if the extension is really py
         get_filename_component( _fileext "${_file}" EXT )
         if( NOT ${_fileext} STREQUAL ".py" )
            message( STATUS "Ignoring file ${_file} with extension ${_fileext}" )

            unset( _fileext )

            continue()
         endif()

         # Get the file's name, without its (probably) .py extension:
         get_filename_component( _filename "${_file}" NAME_WE )
         # Construct the bytecode's file name:
         set( _bytecode
            "${CMAKE_PYTHON_OUTPUT_DIRECTORY}/${ARG_PACKAGE}/${_filename}.pyc" )

         # The python command to execute:
         set( _pycmd "import py_compile; py_compile.compile( " )
         set( _pycmd "${_pycmd}'${CMAKE_CURRENT_SOURCE_DIR}/${_file}', " )
         set( _pycmd "${_pycmd}cfile = '${_bytecode}', doraise = True )" )

         # Generate a python file with this content. Unfortunately this is
         # necessary, because in CTEST_USE_LAUNCHERS=TRUE mode the quotes in
         # the custom command would be swallowed.
         string( REGEX REPLACE "(\\.|\\/)" "_" _bytecodeFname "${_filename}" )
         set( _pycmdFile "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${ARG_PACKAGE}" )
         set( _pycmdFile "${_pycmdFile}/${_bytecodeFname}pycGen.py" )
         file( GENERATE OUTPUT ${_pycmdFile}
            CONTENT "${_pycmd}" )

         # Set up a custom command for generating the bytecode:
         add_custom_command( OUTPUT ${_bytecode}
            COMMAND ${CMAKE_COMMAND} -E make_directory
            ${CMAKE_PYTHON_OUTPUT_DIRECTORY}/${ARG_PACKAGE}
            COMMAND ${PYTHON_EXECUTABLE} ${_pycmdFile}
            DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/${_file}"
            VERBATIM )

         # Make our installation target call this custom command:
         set_property( TARGET ${_tgtName} APPEND PROPERTY
            INSTALLED_FILES ${_bytecode} )

         # Install this file:
         install( FILES ${_bytecode}
            DESTINATION ${CMAKE_INSTALL_PYTHONDIR}/${ARG_PACKAGE} )

         # Remember to clean out these files when asked:
         set_property( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} APPEND PROPERTY
            ADDITIONAL_MAKE_CLEAN_FILES ${_bytecode} ${_pycmdFile} )

         # Clean up:
         unset( _fileext )
         unset( _filename )
         unset( _bytecode )
         unset( _pycmd )
         unset( _bytecodeFname )
         unset( _pycmdFile )

      endif()

   endforeach()

   # Clean up:
   unset( _files )
   unset( _tgtName )

endfunction( tn_install_python_module )

# This function installs binaries from the package into the
# right place in both the build and the install directories.
#
# Based on atlas_install_python_modules.
#
# Usage: tn_install_binary( bin/someBin )
#
function( tn_install_binary )
    
   cmake_parse_arguments( ARG "" "" "" ${ARGN} )

   # Call the generic function:
   tn_install_generic( ${ARG_UNPARSED_ARGUMENTS}
      DESTINATION ${CMAKE_INSTALL_BINDIR}
      BUILD_DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}
      TYPENAME Binary
      EXECUTABLE )

endfunction( tn_install_binary )

# This function installs libraries from the package into the
# right place in both the build and the install directories.
#
# Based on atlas_install_python_modules.
#
# Usage: tn_install_library( lib/someLib )
#
function( tn_install_library )
    
   cmake_parse_arguments( ARG "" "" "" ${ARGN} )

   # Call the generic function:
   tn_install_generic( ${ARG_UNPARSED_ARGUMENTS}
      DESTINATION ${CMAKE_INSTALL_LIBDIR}
      BUILD_DESTINATION ${CMAKE_LIBRARY_OUTPUT_DIRECTORY}
      TYPENAME Library )

endfunction( tn_install_library )


# This function installs data from the package into the
# right place in both the build and the install directories.
#
# Based on atlas_install_python_modules.
#
# Usage: tn_install_data( someFile )
#
function( tn_install_data )

   cmake_parse_arguments( ARG "" "" "" ${ARGN} )

   # Call the generic function:
   tn_install_generic( ${ARG_UNPARSED_ARGUMENTS}
      DESTINATION ${CMAKE_INSTALL_DATADIR}
      BUILD_DESTINATION ${CMAKE_DATA_OUTPUT_DIRECTORY}
      TYPENAME Data )

endfunction( tn_install_data )
