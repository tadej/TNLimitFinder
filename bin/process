#!/bin/bash
#
# Prepare, run and post-process the jobs
#

if [[ -z ${TNLIMITFINDER+x} ]]; then
  echo "Error: run TNLimitFinder setup scripts before running" 1>&2
  exit 1
fi

export TASK_DESCRIPTION="Processing jobs"

# shellcheck source=scripts/common/task.sh
source "$TNLIMITFINDER/scripts/common/task.sh"

if [[ -z ${OPT_processing_type+x} ]]; then
  OPT_processing_type="${FIT_DEFAULT_PROCESSING_TYPE}"
fi

if [[ "${OPT_processing_type}" != "act" ]] && [[ "${OPT_processing_type}" != "local" ]] && [[ "${OPT_processing_type}" != "grid" ]]; then
  log_error "Processing type '${OPT_processing_type}' is not recognised. Use 'act', 'grid' or 'local'."
  exit 1
fi

log_info "Using processing type '$OPT_processing_type'"
echo

# shellcheck source=scripts/processing/prepare.sh
source "$SCRIPTS_DIR/processing/prepare.sh"
# shellcheck source=scripts/processing/process.sh
source "$SCRIPTS_DIR/processing/process.sh"
# shellcheck source=scripts/processing/merge.sh
source "$SCRIPTS_DIR/processing/merge.sh"

# Prepare jobs configuration
prepare_jobs_all

# Do the actual processing
process_"$OPT_processing_type"

# Post-process and merge
if [[ "${OPT_processing_type}" != "grid" ]]; then
  merge_outputs
fi

echo

# Cleanup
prepare_cleanup

# We have success!
log_success "All done successfully"
