#
# Parse command line options
#

getopt --test > /dev/null
if [[ $? -ne 4 ]]; then
  log_error "$(getopt --test) failed in this environment."
  exit 1
fi

# general options
OPT_toys=0
OPT_input=""
OPT_revision=1
OPT_offset=0
OPT_debug=0

opt_short=h,f
opt_long=help,frequentist

if [[ "$CMD_SCRIPT" == "process" ]]; then
  opt_short=$opt_short,t:,r:,i:,o:
  opt_long=$opt_long,type:,revision:,input:,offset:
  EXTRA_HELP=$'  -t, --type TYPE         processing type, \'act\', \'grid\' or \'local\'\n  -i, --input SUBSTRING   input substring\n  -o, --offset NUM        job seed offset\n  -r, --revision REV      production revision'
elif [[ "$CMD_SCRIPT" == "analyze" ]] || [[ "$CMD_SCRIPT" == "grid_merge" ]] || [[ "$CMD_SCRIPT" == "export_limits" ]]; then
  opt_short=$opt_short,i:
  opt_long=$opt_long,input:
  EXTRA_HELP=$'  -i, --input SUBSTRING   input substring'
elif [[ "$CMD_SCRIPT" == "test_run" ]]; then
  opt_short=$opt_short,d
  opt_long=$opt_long,debug
  EXTRA_HELP=$'  -d, --debug             run with gdb'
fi

# -temporarily store output to be able to check for errors
# -activate advanced mode getopt quoting e.g. via “--options”
# -pass arguments only via   -- "$@"   to separate them correctly
if ! opt_parsed=$(getopt --options $opt_short --longoptions $opt_long --name "$0" -- "$@"); then
  echo "see '$CMD_SCRIPT --help' for more information" 1>&2
  exit 2
fi
# use eval with "$opt_parsed" to properly handle the quoting
eval set -- "$opt_parsed"

# now enjoy the options in order and nicely split until we see --
while true; do
  case "$1" in
    -h|--help)
      args="<jobName> ..."
      # shellcheck disable=SC2154
      echo "${color_blue}TNLimitFinder${color_reset_all} $CMD_SCRIPT"
      echo "Usage: $CMD_SCRIPT [options] $args"
      echo
      echo "Options:"
      echo "  -h, --help              show this help message and exit"
      echo "  -f, --frequentist       run with toys"
      if [[ -n ${EXTRA_HELP+x} ]]; then
        echo "$EXTRA_HELP"
      fi
      exit 0
      ;;
    -t|--type)
      OPT_processing_type=$2
      shift 2
      ;;
    -f|--frequentist)
      OPT_toys=1
      shift
      ;;
    -i|--input)
      OPT_input=$2
      shift 2
      ;;
    -r|--revision)
      OPT_revision=$2
      shift 2
      ;;
    -o|--offset)
      OPT_offset=$2
      shift 2
      ;;
    -d|--debug)
      OPT_debug=1
      shift
      ;;
    --)
      shift
      break
      ;;
    *)
      log_error "Programming error"
      exit 3
      ;;
  esac
done

# export options
export OPT_toys
export OPT_processing_type
export OPT_input
export OPT_revision
export OPT_offset
export OPT_debug
