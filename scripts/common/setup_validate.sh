#
# Validate setup
#

TNLIMITFINDER_SETUP_ERROR=0

if [[ -z ${FIT_CONFIG_DIR+x} ]]; then
  log_error "Fit configuration path (FIT_CONFIG_DIR) is not defined"
  TNLIMITFINDER_SETUP_ERROR=1
elif [[ "${FIT_CONFIG_DIR: -1}" == "/" ]]; then
  log_error "Fit configuration path (FIT_CONFIG_DIR) should not end with a trailing slash '/'"
  TNLIMITFINDER_SETUP_ERROR=1
elif [[ ! -d "${FIT_CONFIG_DIR}" ]]; then
  log_error "Fit configuration path (FIT_CONFIG_DIR) does not exist"
  TNLIMITFINDER_SETUP_ERROR=1
fi

if [[ -z ${FIT_INPUT_DIR+x} ]]; then
  log_error "Fit input path (FIT_INPUT_DIR) is not defined"
  TNLIMITFINDER_SETUP_ERROR=1
elif [[ "${FIT_INPUT_DIR: -1}" == "/" ]]; then
  log_error "Fit input path (FIT_INPUT_DIR) should not end with a trailing slash '/'"
  TNLIMITFINDER_SETUP_ERROR=1
elif [[ ! -d "${FIT_INPUT_DIR}" ]]; then
  log_error "Fit input path (FIT_INPUT_DIR) does not exist"
  TNLIMITFINDER_SETUP_ERROR=1
fi

if [[ -z ${FIT_OUTPUT_DIR+x} ]]; then
  log_error "Fit workspace (FIT_OUTPUT_DIR) is not defined"
  TNLIMITFINDER_SETUP_ERROR=1
elif [[ "${FIT_OUTPUT_DIR: -1}" == "/" ]]; then
  log_error "Fit workspace (FIT_OUTPUT_DIR) should not end with a trailing slash '/'"
  TNLIMITFINDER_SETUP_ERROR=1
fi

if [[ -n ${FIT_DEFAULT_PROCESSING_TYPE+x} ]] && [[ "${FIT_DEFAULT_PROCESSING_TYPE}" != "local" ]] && [[ "${FIT_DEFAULT_PROCESSING_TYPE}" != "act" ]] && [[ "${FIT_DEFAULT_PROCESSING_TYPE}" != "grid" ]]; then
  log_error "Default processing type (FIT_DEFAULT_PROCESSING_TYPE) is not recognised. Use 'act', 'grid' or 'local'."
  TNLIMITFINDER_SETUP_ERROR=1
fi

export TNLIMITFINDER_SETUP_ERROR
