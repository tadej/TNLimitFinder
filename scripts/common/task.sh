#
# Common task base
#

# shellcheck source=scripts/common/base.sh
source "$TNLIMITFINDER/scripts/common/base.sh"
# shellcheck source=scripts/common/options.sh
source "$SCRIPTS_DIR/common/options.sh"
# shellcheck source=scripts/jobs/init.sh
source "$SCRIPTS_DIR/jobs/init.sh"
