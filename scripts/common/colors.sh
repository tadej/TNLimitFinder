#
# Colors definitions
#
# shellcheck disable=SC2034
# Colors
color_red="$(tput setaf 1)"
color_green="$(tput setaf 2)"
color_yellow="$(tput setaf 3)"
color_blue="$(tput setaf 4)"
color_magenta="$(tput setaf 5)"
color_cyan="$(tput setaf 6)"
color_white="$(tput setaf 7)"
color_gray="$(tput setaf 8)"
color_light_red="$(tput setaf 9)"
color_light_green="$(tput setaf 10)"
color_light_yellow="$(tput setaf 11)"
color_light_blue="$(tput setaf 12)"
color_light_magenta="$(tput setaf 13)"
color_light_cyan="$(tput setaf 14)"
color_light_white="$(tput setaf 15)"
color_light_gray="$(tput setaf 16)"

color_reset_all="$(tput sgr0)"
