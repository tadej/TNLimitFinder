#
# Common scripts logging
#

# Forward declarations
declare color_green > /dev/null
declare color_magenta > /dev/null
declare color_red > /dev/null
declare color_light_red > /dev/null
declare color_light_yellow > /dev/null
declare color_reset_all > /dev/null

# Colors
# shellcheck source=scripts/common/colors.sh
source "$TNLIMITFINDER/scripts/common/colors.sh"

# Helper functions
log_error() {
  echo "${color_red}$CMD_SCRIPT error:${color_light_red} $1${color_reset_all}" 1>&2
}

log_info() {
  echo "${color_light_yellow}$1${color_reset_all}"
}

log_warning() {
  echo "${color_magenta}Warning:${color_reset_all} $1" 1>&2
}

log_success() {
  echo "${color_green}$1${color_reset_all}"
}

log_failure() {
  echo "${color_red}$1${color_reset_all}$2" 1>&2
}
