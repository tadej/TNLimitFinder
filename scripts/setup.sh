#
# Setup the working environment
#

# Bash location
SETUP_LOCATION=${BASH_SOURCE[0]}
# Zsh fallback
if [[ -z ${BASH_SOURCE[0]+x} ]]; then
  SETUP_LOCATION=${(%):-%N}
  ZSH_COMPLETIONS=1
fi

TNLIMITFINDER=$(cd "$(dirname "${SETUP_LOCATION}")" && pwd)
TNLIMITFINDER_VERSION=$(cat "$TNLIMITFINDER/VERSION")
TNLIMITFINDER_LCG_PLATFORM="x86_64-centos7-gcc8-opt"
TNLIMITFINDER_ROOT_VERSION="6.20.02-x86_64-centos7-gcc8-opt"
TNLIMITFINDER_ROOT_VERSION_GRID="6.20/02"

# shellcheck source=scripts/common/logging.sh
source "$TNLIMITFINDER/scripts/common/logging.sh"
# shellcheck source=scripts/common/setup_validate.sh
source "$TNLIMITFINDER/scripts/common/setup_validate.sh"

# do the setup only if valid
if [[ $TNLIMITFINDER_SETUP_ERROR -eq 0 ]]; then

if [[ -z ${FIT_DEFAULT_PROCESSING_TYPE+x} ]]; then
  export FIT_DEFAULT_PROCESSING_TYPE="local"
fi

echo "${color_blue}TNLimitFinder ${TNLIMITFINDER_VERSION}${color_reset_all}"
log_info "Setting up TNLimitFinder"
echo
echo "Using TNLimitFinder from: $TNLIMITFINDER"
echo "Using config directory: $FIT_CONFIG_DIR"
echo "Using input directory: $FIT_INPUT_DIR"
echo "Using working directory: $FIT_OUTPUT_DIR"
if [[ -n ${FIT_EXPORT_DIR+x} ]]; then
  echo "Using export directory: $FIT_EXPORT_DIR"
fi
echo
echo "Using default processing type: $FIT_DEFAULT_PROCESSING_TYPE"
if [[ -n ${FIT_ACT_SITE+x} ]]; then
  echo "aCT site: $FIT_ACT_SITE"
fi
if [[ -n ${FIT_CONTAINER_IMAGE+x} ]]; then
  echo
  echo "Using container: $FIT_CONTAINER_IMAGE"
  if [[ -x $(command -v singularity) ]]; then
    export TNLIMITFINDER_SINGULARITY=1
  else
    export TNLIMITFINDER_SINGULARITY=0
    echo "Warning: Singularity not available locally. A local build is needed to run locally." 1>&2
  fi
fi
echo

if [[ ":$PATH:" != *":$TNLIMITFINDER/bin:"* ]];
then
  export PATH="$TNLIMITFINDER/bin:$PATH"
  echo "Added '\$TNLIMITFINDER/bin' to \$PATH"
fi

if [[ $ZSH_COMPLETIONS -eq 1 ]] && [[ ! ${fpath[(i)$TNLIMITFINDER/scripts/completions]} -le ${#fpath} ]]; then
  fpath+=("$TNLIMITFINDER/scripts/completions")
  compinit -u
  echo "Added '\$TNLIMITFINDER/scripts/completions' to \$fpath"
fi

# macOS specifics
if [[ -d "/usr/local/opt/gnu-getopt/bin" && ":$PATH:" != *":/usr/local/opt/gnu-getopt/bin:"* ]];
then
  export PATH="/usr/local/opt/gnu-getopt/bin:$PATH"
  echo "Added '/usr/local/opt/gnu-getopt/bin' to \$PATH"
fi

# prll
export PRLL_HELPER_PATH="$TNLIMITFINDER/bin"
export PRLL_SOURCE="$TNLIMITFINDER/scripts/setup/prll.sh"

# export the variables
export TNLIMITFINDER
export TNLIMITFINDER_VERSION
export TNLIMITFINDER_LCG_PLATFORM
export TNLIMITFINDER_ROOT_VERSION
export TNLIMITFINDER_ROOT_VERSION_GRID

fi
# end of setup check

unset TNLIMITFINDER_SETUP_ERROR
