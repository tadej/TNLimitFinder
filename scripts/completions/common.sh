__TN_jobs() {
  setopt shwordsplit

  jobs=(all)

  local f files job
  files=$(find "$FIT_CONFIG_DIR" -name '*.json')
  for f in $files; do
    if [[ -f ${f} ]]; then
      job=${f[(ws:/:)-1]}
      job=${job//\.json/}

      jobs+=("$job")
    fi
  done
}
