function prepare_tar() {
  pushd "$TNLIMITFINDER" > /dev/null || exit
  tar \
    --exclude='.[^/]*' \
    --exclude='*.pyc' \
    --exclude='*.dbg' \
    -chzf "$1" \
    bin lib
  popd > /dev/null || exit
}

function prepare_jobs_all() {
  local processing=""

  for j in $VAR_jobs; do
    job_info "$j"
    job_config "$j"

    log_info "Preparing config and code for '$j'"

    processing="$processing $j"

    if [[ $(find "$OUTPUT_JOBS/definitions" -mindepth 1 -maxdepth 1 | wc -l) -eq 0 ]]; then
      if [[ ! $TNLIMITFINDER_SINGULARITY -eq 1 ]]; then
        if [[ -n ${FIT_TEMP_OUTPUT_DIR+x} ]]; then
          prepare_tar "$FIT_TEMP_OUTPUT_DIR/$j/TNLimitFinder.tar.gz"
        else
          prepare_tar "$OUTPUT_BASE/TNLimitFinder.tar.gz"
        fi
      fi

      prepare_jobs "$j"

      log_success "Done successfully"
    else
      log_success "Job configurations already present. Continuing..."
    fi

    echo
  done

  export VAR_processing_jobs=$processing
}

function prepare_jobs() {
  local job=$1
  local input VAR_inputs
  list_inputs

  local extra=""
  if [[ $OPT_toys -eq 1 ]]; then
    extra="$extra --toys"
  fi

  local N

  local prefix="TNLF_${job}"
  local suffix
  if [[ $OPT_toys -eq 1 ]]; then
    suffix="freq"
    N=$(get_jobs)
  else
    suffix="asym"
    N=1
  fi

  local name local_extra
  for input in $VAR_inputs; do
    if [[ "$input" != *"$OPT_input"* ]]; then
      continue
    fi

    local i
    local input_short=${input//$FIT_INPUT_DIR\//}
    local output_short=${input_short//\.root/_asym.1.root}
    local_extra=""
    if [[ $OPT_toys -eq 1 ]]; then
      if [[ ! -f "$OUTPUT_RESULTS/$output_short" ]]; then
        log_error "Asymptotic result '$output_short' does not exist"
        exit 10
      fi
      local_extra="$(get_POI_range "$OUTPUT_RESULTS/$output_short")"
    fi

    local command command_standard submission_extra
    if [[ "${OPT_processing_type}" == "grid" ]]; then
      echo "Job for '$input_short'"
      name="user.${USER}.${prefix}.${input_short//\.root/}.${suffix}.o${OPT_offset}.fit.r${OPT_revision}"
      echo "$name"
      command="./grid_wrapper /opt/TNLimitFinder/bin/tn_fit config.json %RNDM:${OPT_offset} input.root result.root $extra $local_extra"
      define_job_grid "$name" "$command" "$N" "$input"
    else
      for i in $(seq 1 "$N"); do
        echo "Job #$((i+OPT_offset)) for '$input_short'"

        name="${prefix}_${input_short//\.root/}_${suffix}_$((i+OPT_offset))"

        command_standard="code/bin/tn_fit config.json $((i+OPT_offset)) \"$input\" result.root $extra $local_extra"
        if [[ -n ${FIT_CONTAINER_IMAGE+x} ]]; then
          command="/opt/TNLimitFinder/bin/tn_fit config.json $((i+OPT_offset)) input.root result.root $extra $local_extra"
        else
          command="$command_standard"
        fi
        # shellcheck disable=SC2154
        if [[ "${OPT_processing_type}" == "act" ]]; then
          define_job_arc "$name" "$command" "$submission_extra"
          submission_extra="$input"
        elif [[ "${OPT_processing_type}" == "local" ]]; then
          if [[ -n ${FIT_CONTAINER_IMAGE+x} ]]; then
            command="${command//input.root/\"$input\"}"
          fi
          define_job_local "$name" "$command"
        fi
      done
    fi
    echo
  done
}

function prepare_cleanup() {
  echo "Cleaning..."

  for j in $VAR_processing_jobs; do
    job_info "$j"

    if [[ ! $(find "$OUTPUT_JOBS/definitions/" -mindepth 1 -maxdepth 1 | wc -l) -eq 0 ]]; then
      for d in "$OUTPUT_JOBS/definitions"/*; do
        rm -r "$d"
      done
    fi

    if [[ ! $(find "$OUTPUT_JOBS_DOWNLOAD/" -mindepth 1 -maxdepth 1 | wc -l) -eq 0 ]]; then
      for d in "$OUTPUT_JOBS_DOWNLOAD/TNLF_${j}"*; do
        if [[ -d "$d" ]]; then
          rm -r "$d"
        fi
      done
    fi  
  done
}

function define_job_arc() {
  local name=$1
  local command=$2
  local submission_extra=$3

  local output="$OUTPUT_JOBS_DOWNLOAD/$name"
  
  local spec="$OUTPUT_JOBS/definitions/$name.xrsl"

  {
    echo "&"
    echo "(executable=\"cluster_run\")"
    if [[ -n ${FIT_CONTAINER_IMAGE+x} ]]; then
      echo "(arguments='$command')"
      echo "(inputFiles=(\"cluster_run\" \"$TNLIMITFINDER/scripts/submission/arc_run_container\")(\"config.json\" \"$OUTPUT_BASE/config.json\")(\"input.root\" \"$submission_extra\"))"
      echo "(outputFiles=(\"result.root\" \"\"))"
      echo "(runtimeenvironment = ENV/SINGULARITY $FIT_CONTAINER_IMAGE)"
    elif [[ -z ${FIT_TEMP_OUTPUT_DIR+x} ]]; then
      echo "(arguments='$TNLIMITFINDER_ROOT_VERSION' '$command')"
      echo "(inputFiles=(\"cluster_run\" \"$TNLIMITFINDER/scripts/submission/arc_run\")(\"config.json\" \"$OUTPUT_BASE/config.json\")(\"TNLimitFinder.tar.gz\" \"$OUTPUT_BASE/TNLimitFinder.tar.gz\"))"
      echo "(outputFiles=(\"result.root\" \"\"))"
      echo "(runtimeenvironment = APPS/HEP/ATLAS-SITE)"
    else
      echo "(arguments='$TNLIMITFINDER_ROOT_VERSION' '$command' '$OUTPUT_JOBS_DOWNLOAD' '$output')"
      echo "(inputFiles=(\"cluster_run\" \"$TNLIMITFINDER/scripts/submission/arc_run\")(\"config.json\" \"$OUTPUT_BASE/config.json\"))"
      echo "(runtimeenvironment = APPS/HEP/ATLAS-SITE)"
    fi
    echo "(jobName=\"$name\")"
    echo "(memory=2000)"
    echo "(join=yes)"
    echo "(stdout=\"run.out\")"
    echo "(cpuTime=\"1440\")"
    echo "(wallTime=\"1440\")"
  } > "$spec"
}

function define_job_local() {
  local name=$1
  local command=$2

  local folder="$OUTPUT_JOBS/definitions/$name"
  local spec="$folder/run.sh"

  if [[ ! -d "$folder" ]]; then
    mkdir -p "$folder"
  fi

  command=${command//code\/bin\//}
  command=${command//config\.json/\"$OUTPUT_BASE\/config.json\"}

  if [[ $TNLIMITFINDER_SINGULARITY -eq 1 ]]; then
    command="${command//result.root/$folder\/result.root}"
    command="$(singularity_exec_echo) ${command}"
  fi

  echo "$command" > "$spec"
  chmod +x "$spec"
}

function define_job_grid() {
  local name=$1
  local command=$2
  local N=$3
  local input=$4
  local nJobs="nJobs"
  local escaped_command escaped_merge_command escaped_image
  escaped_command=$(echo "$command" | sed -e 's/[\/&]/\\&/g')
  escaped_merge_command=$(echo "./grid_wrapper /opt/TNLimitFinder/bin/tn_merge %OUT %IN" | sed -e 's/[\/&]/\\&/g')
  escaped_image=$(echo "$FIT_CONTAINER_IMAGE" | sed -e 's/[\/&]/\\&/g')

  local folder="$OUTPUT_JOBS/definitions/$name"

  if [[ ! -d "$folder" ]]; then
    mkdir -p "$folder"
  fi

  if [[ -f ${input} ]]; then
    cp "$OUTPUT_BASE/config.json" "$folder/config.json"
    cp "$input" "$folder/input.root"
  fi

  sed -e "s/JOB_NAME/${name}/g" \
    -e "s/JOB_IMAGE/${escaped_image}/g" \
    -e "s/JOB_COUNT/${N}/g" \
    -e "s/JOB_COMMAND/${escaped_command}/g" \
    -e "s/JOB_MERGE_COMMAND/${escaped_merge_command}/g" \
    -e "s/--nJobs/--${nJobs}/g" \
    "$TNLIMITFINDER/scripts/submission/grid_submit" > "$folder/grid_submit"

  cp "$TNLIMITFINDER/scripts/submission/grid_run" "$folder/grid_run"
  cp "$TNLIMITFINDER/scripts/submission/grid_wrapper" "$folder/grid_wrapper"

  chmod +x "$folder/grid_submit"
  chmod +x "$folder/grid_run"
  chmod +x "$folder/grid_wrapper"
}
