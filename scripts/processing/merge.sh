function merge_outputs()
{
  local j
  for j in $VAR_jobs; do
    job_info "$j"
    job_config "$j"
    
    validate_output "$j"

    if [[ $failed ]]; then
      log_failure "Missing files summary"
      for f in "${failed[@]}"; do
        echo "$f" 1>&2
      done
      exit 5
    fi

    local suffix
    if [[ $OPT_toys -eq 1 ]]; then
      suffix="freq"
    else
      suffix="asym"
    fi

    local input VAR_inputs
    list_inputs
    for input in $VAR_inputs; do
      if [[ "$input" != *"$OPT_input"* ]]; then
        continue
      fi

      local input_short=${input//$FIT_INPUT_DIR\//}
      local output_short=${input_short//\.root/_${suffix}.root}
      input_short=${input_short//\.root/}

      local samples max
      samples="$(find $OUTPUT_JOBS_DOWNLOAD/TNLF_${j}_${input_short}_* -iname result.root | sort)"
      if [[ $OPT_toys -eq 1 ]]; then
        max=500
      else
        max=50
      fi

      local sample i tmp_list
      i=1
      tmp_list=()
      for sample in $samples; do
        tmp_list+=("$sample")

        if [[ ${#tmp_list[@]} -eq $max ]]; then
          if [[ $TNLIMITFINDER_SINGULARITY -eq 1 ]]; then
            singularity_exec "/opt/TNLimitFinder/bin/tn_merge" "$OUTPUT_RESULTS/${output_short//\.root/.${i}.root}" "${tmp_list[@]}"
          else
            tn_merge "$OUTPUT_RESULTS/${output_short//\.root/.${i}.root}" "${tmp_list[@]}"
          fi

          tmp_list=()
          i=$((i+1))
        fi
      done

      if [[ ${#tmp_list[@]} -gt 0 ]]; then
        if [[ $TNLIMITFINDER_SINGULARITY -eq 1 ]]; then
          singularity_exec "/opt/TNLimitFinder/bin/tn_merge" "$OUTPUT_RESULTS/${output_short//\.root/.${i}.root}" "${tmp_list[@]}"
        else
          tn_merge "$OUTPUT_RESULTS/${output_short//\.root/.${i}.root}" "${tmp_list[@]}"
        fi
      fi
    done
  done
}

function validate_output()
{
  local folder="$OUTPUT_JOBS/definitions"

  local f
  for f in $(find "$folder" -mindepth 1 -maxdepth 1 -iname "TNLF_${1}_*" | sort); do
    f=${f//$folder\//}
    f=${f//\.xrsl/}

    local job_folder="$OUTPUT_JOBS_DOWNLOAD/$f"

    if [[ -d "$job_folder" ]]; then
      if [[ ! -f "$job_folder/result.root" ]] || [[ ! -s "$job_folder/result.root" ]]; then
        log_warning "$f has no output"
        failed+=("$f")
      fi
    else
      log_warning "$f has no output (no job folder)"
      failed+=("$f")
    fi
  done
}
