function process_act()
{
  log_info "Processing datasets using ACT"

  local all=0
  local j
  for j in $VAR_processing_jobs; do
    local current_all=${j}_all

    job_info "$j"

    declare "$current_all"=0 > /dev/null
    local argf=()
    local definitions
    definitions=$(find "$OUTPUT_JOBS/definitions" -type f -name 'TNLF_*.xrsl')
    for d in $definitions; do
      argf+=("$d")
      declare "$current_all"=$((${!current_all}+1)) > /dev/null
      all=$((all+1))
    done

    echo "Job count for '$j': ${!current_all}"

    local count_curr downloaded_curr
    count_curr=$(actstat -a -f "_${j}_" | tail -n+3 | wc -l)
    downloaded_curr=$(find "$OUTPUT_JOBS_DOWNLOAD" -mindepth 1 -maxdepth 1 -iname "*_${j}_*" | wc -l)
    count_curr=$((count_curr+downloaded_curr))
    if [[ $count_curr -eq 0 ]]; then
      echo "Submitting..."
      if [[ -z ${FIT_ACT_SITE+x} ]]; then
        actbulksub "${argf[@]}"
      else
        actbulksub "${argf[@]}" -s "${FIT_ACT_SITE}"
      fi
    else
      echo "Already submitted"
    fi
  done

  echo "Total submitted jobs: $all"
  echo

  local submitted=0
  local running=0
  local finished=0
  local downloaded=0
  local failed_c=0
  until [[ $downloaded -eq $all ]] && [[ $running -eq 0 ]] && [[ $finished -eq 0 ]] && [[ $submitted -eq 0 ]] && [[ $failed_c -eq 0 ]] ; do
    submitted=0
    running=0
    finished=0
    downloaded=0
    failed_c=0

    echo "Time: $(date '+%T')"

    for j in $VAR_processing_jobs; do
      local current_all=${j}_all

      job_info "$j"

      local submitted_curr running_curr done_curr finished_curr downloaded_curr failed_curr
      submitted_curr=$(actstat -a -f "_${j}_" -s "submitted" | tail -n+3 | wc -l)
      running_curr=$(actstat -a -f "_${j}_" -s "running" | tail -n+3 | wc -l)
      done_curr=$(actstat -a -f "_${j}_" -s "done" | tail -n+3 | wc -l)
      finished_curr=$(actstat -a -f "_${j}_" -s "finished" | tail -n+3 | wc -l)
      downloaded_curr=$(find "$OUTPUT_JOBS_DOWNLOAD" -mindepth 1 -maxdepth 1 -iname "*_${j}_*" | wc -l)
      failed_curr=$(actstat -a -f "_${j}_" -s "failed" | tail -n+3 | wc -l)

      echo "$j: $downloaded_curr/$running_curr/$submitted_curr/${!current_all}/$failed_curr"

      submitted=$((submitted+submitted_curr))
      running=$((running+running_curr))
      finished=$((finished+finished_curr))
      downloaded=$((downloaded+downloaded_curr))
      failed_c=$((failed_c+failed_curr))

      if [[ ! $done_curr -eq 0 ]]; then
        if [[ -n ${FIT_CONTAINER_IMAGE+x} ]]; then
          pushd "$OUTPUT_JOBS_DOWNLOAD" > /dev/null || exit
          actget -a -f "_${j}_" -s "done"
          popd > /dev/null || exit
        else
          actclean -a -f "_${j}_" -s "done" > /dev/null
        fi
      fi
    done

    echo "Total progress: $downloaded/$running/$submitted/$all/$failed_c"
    echo

    if [[ ! $downloaded -eq $all ]] || [[ ! $running -eq 0 ]] || [[ ! $finished -eq 0 ]] || [[ ! $failed_c -eq 0 ]] || [[ ! $submitted -eq 0 ]]; then
      sleep 15
    fi
  done

  log_success "All jobs done."
  echo

  for j in $VAR_processing_jobs; do
    actclean -a -f "_${j}_"
  done
}

function run_local()
{
  log_info "Executing ${1//$OUTPUT_JOBS\/definitions\//}"

  pushd "$1" > /dev/null || exit

  if [[ -f result.root ]]; then
    echo "  Already done."
  else
    ./run.sh
  fi

  popd > /dev/null || exit
}

function process_local() {
  log_info "Processing datasets locally"

  local list=""
  local j
  for j in $VAR_processing_jobs; do
    job_info "$j"

    list="$list $(find "$OUTPUT_JOBS/definitions" -mindepth 1 -maxdepth 1 -type d -iname "TNLF_${j}*")"
  done

  # shellcheck disable=SC1090
  source "$PRLL_SOURCE"
  prll run_local $list

  for j in $VAR_processing_jobs; do
    job_info "$j"

    local download
    download=$(find "$OUTPUT_JOBS/definitions" -mindepth 1 -maxdepth 1 -type d -iname "TNLF_${j}*")

    for d in $download; do
      local out="$OUTPUT_JOBS_DOWNLOAD/${d/$OUTPUT_JOBS\/definitions\//}"

      mkdir -p "$out"
      cp "$d/result.root" "$out"
    done
  done

  log_success "All jobs done."
}

function submit_grid_job()
{
  log_info "Submitting ${1//$OUTPUT_JOBS\/definitions\//}"

  pushd "$1" > /dev/null || exit
  ./grid_submit
  popd > /dev/null || exit
}

function process_grid() {
  log_info "Processing datasets on the grid"

  local job_list=""
  for j in $VAR_processing_jobs; do
    job_info "$j"

    job_list="$job_list $(find "$OUTPUT_JOBS/definitions" -mindepth 1 -maxdepth 1 -type d -iname "user.${USER}*")"
  done

  local j
  for j in $job_list; do
    submit_grid_job "$j"
  done

  log_success "All jobs submitted."
  echo
}
