#
# Common jobs functions
#

function job_welcome()
{
  # shellcheck disable=SC2154
  echo "${color_blue}TNLimitFinder${color_reset_all}"

  if [[ -n ${TASK_DESCRIPTION+x} ]]; then
    log_info "$TASK_DESCRIPTION"
  else
    log_info "Running $CMD_SCRIPT"
  fi
  echo
}

function job_start_message()
{
  log_info "Running for job configuration '$1'"
}

function job_start_input_message()
{
  log_info "Processing input '$1'"
}

function job_config()
{
  cp "$FIT_CONFIG_DIR/$1.json" "$OUTPUT_BASE/config.json"
}

function job_info()
{
  # TODO
  local out
  out=${1}
  if [[ ! -f "$FIT_CONFIG_DIR/$1.json" ]]; then
    log_error "Job '$1' does not exist"
    exit 5
  fi

  OUTPUT_BASE="$FIT_OUTPUT_DIR/$out"
  OUTPUT_RESULTS="$OUTPUT_BASE/results"
  OUTPUT_JOBS="$OUTPUT_BASE/jobs"
  OUTPUT_TEST="$OUTPUT_BASE/test"

  if [[ -n ${FIT_TEMP_OUTPUT_DIR+x} ]]; then
    OUTPUT_JOBS_DOWNLOAD="$FIT_TEMP_OUTPUT_DIR/$1"
    if [[ ! -d "$OUTPUT_JOBS_DOWNLOAD" ]]; then
      mkdir -p "$OUTPUT_JOBS_DOWNLOAD"
      chmod a+wr "$OUTPUT_JOBS_DOWNLOAD"
    fi
  else
    OUTPUT_JOBS_DOWNLOAD="$OUTPUT_JOBS/download"
  fi
}

function job_prepare()
{
  if [[ ! -d "$OUTPUT_RESULTS" ]]; then
    mkdir -p "$OUTPUT_RESULTS"
  fi

  if [[ ! -d "$OUTPUT_JOBS/definitions" ]]; then
    mkdir -p "$OUTPUT_JOBS/definitions"
  fi

  if [[ ! -d "$OUTPUT_JOBS/download" ]]; then
    mkdir -p "$OUTPUT_JOBS/download"
  fi

  if [[ ! -d "$OUTPUT_TEST" ]]; then
    mkdir -p "$OUTPUT_TEST"
  fi
}

function job_execute_all()
{
  local failed=""

  local j
  for j in "$@"; do
    job_start_message "$j"

    job_info "$j"
    job_config "$j"

    if ! task_command "$j"; then
      failed="$failed $j"
    fi

    echo
  done

  if [[ ${failed} = "" ]]; then
    log_success "All jobs done successfully"
    exit 0
  else
    log_failure "Failed:" "$failed"
    exit 20
  fi
}

function list_inputs()
{
  local inputs
  inputs=$(find "$FIT_INPUT_DIR" -iname '*.root' | sort)

  VAR_inputs=$inputs
  export VAR_inputs
}

function get_jobs()
{
  if [[ $TNLIMITFINDER_SINGULARITY -eq 1 ]]; then
    singularity_exec "/opt/TNLimitFinder/bin/tn_jobs" "$OUTPUT_BASE/config.json" | tail -n 1
  else
    tn_jobs "$OUTPUT_BASE/config.json" | tail -n 1
  fi
}

function get_POI_range()
{
  if [[ $TNLIMITFINDER_SINGULARITY -eq 1 ]]; then
    singularity_exec "/opt/TNLimitFinder/bin/tn_range" "$OUTPUT_BASE/config.json" "$1" | tail -n 1
  else
    tn_range "$OUTPUT_BASE/config.json" "$1" | tail -n 1
  fi
}

function singularity_exec()
{
  singularity exec --contain --bind "$FIT_INPUT_DIR":"$FIT_INPUT_DIR" --bind "$OUTPUT_BASE":"$OUTPUT_BASE" "${FIT_CONTAINER_IMAGE}" "$@"
}

function singularity_exec_echo()
{
  echo "singularity exec --contain --bind \"$FIT_INPUT_DIR\":\"$FIT_INPUT_DIR\" --bind \"$OUTPUT_BASE\":\"$OUTPUT_BASE\" \"${FIT_CONTAINER_IMAGE}\""
}
