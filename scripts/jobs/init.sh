#
# Init jobs
#

:

# shellcheck source=scripts/jobs/functions.sh
source "$SCRIPTS_DIR/jobs/functions.sh"

if [[ -z ${1+x} ]]; then
  log_error "At least one job name is required"
  exit 4
fi

job_welcome
VAR_jobs="$*"

for j in $VAR_jobs; do
  job_info "$j"
  job_prepare
  job_config "$j"
done

set -o pipefail

if [[ "$CMD_SCRIPT" = "process" ]] || [[ "$CMD_SCRIPT" = "merge" ]]; then
  # shellcheck disable=SC1090
  source "$PRLL_SOURCE"
fi

if [[ ! "$CMD_SCRIPT" = "process" ]] && [[ ! "$CMD_SCRIPT" = "merge" ]]; then
  job_execute_all $VAR_jobs
fi
