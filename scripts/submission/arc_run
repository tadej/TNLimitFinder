#!/bin/bash
#
# Run job on the arc-based cluster
#

# Node info
hostname
if [[ -f /etc/redhat-release ]]; then
  cat /etc/redhat-release
fi
id

echo

# Prepare ATLAS environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# shellcheck disable=SC1090
source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
lsetup cmake "root $1"

echo

# Extract tar
mkdir code
pushd code > /dev/null || exit
if [[ -n ${3+x} ]]; then
  tar xzvf "$3/TNLimitFinder.tar.gz"
else
  tar xzvf "../TNLimitFinder.tar.gz"
fi
popd > /dev/null || exit

echo

# Run script
TNLIMITFINDER="$(pwd)/code"
export TNLIMITFINDER
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$TNLIMITFINDER/lib"

eval "$2"

code=$?

echo
echo "Finished with exit code: $code"

# Check for return code
if [[ ! $code -eq 0 ]]; then
  exit $code
fi

# Check if output exists
if [[ ! -f result.root ]]; then
  echo "Error: Output file does not exist!"
  exit 100
fi

# Check if output size is non-zero
if [[ ! -s result.root ]]; then
  echo "Error: Output file is empty!"
  exit 200
fi

# Copy output to temp location if needed
if [[ -z ${4+x} ]]; then
  exit 0
fi

if [[ ! -d $4 ]]; then
  mkdir -p "$4"
  chmod a+rxw "$4"
fi

echo "Moving output to $4..."
mv result.root "$4/"
chmod a+wr "$4/result.root"

# Check if output exists
if [[ ! -f "$4/result.root" ]]; then
  echo "Error: Moved file not found!"
  exit 101
fi

# Check if output size is non-zero
if [[ ! -s "$4/result.root" ]]; then
  echo "Error: Moved file is empty!"
  exit 201
fi
