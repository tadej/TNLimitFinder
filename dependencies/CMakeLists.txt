add_definitions(-D_REENTRANT)

set(Dependencies_Srcs
    docopt/docopt.cpp
)

# Define and build the dependencies
add_library(${Dependencies_LIB} STATIC ${Dependencies_Srcs})
set_property(TARGET ${Dependencies_LIB} PROPERTY POSITION_INDEPENDENT_CODE ON)
