# TNLimitFinder

## Configuration

Copy one of the `Example*.sh` files and edit it to your needs.

### Mandatory

- `FIT_CONFIG_DIR`: folder with configuration files
- `FIT_INPUT_DIR`: input files
- `FIT_OUTPUT_DIR`: output folder
- `FIT_BUILD_DIR`: build folder

### Containers

- `FIT_CONTAINER_IMAGE`: singularity container (file or folder)

Prebuilt containers are available on CVMFS for `stable` and `master` branches:

- `/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/tadej/tnlimitfinder:stable`
- `/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/tadej/tnlimitfinder:master`

## Building & Setup

There are two options: run in a container in steering mode (default)
or build TNLimitFinder locally.

CMake 3.11 and ROOT 6.20 are required.
Always make a separate build directory outside the source folder.

```shell
mkdir build && cd build
cmake ../TNLimitFinder
make
```

Setup the environment by sourcing your config file.

```shell
source MyConfig.sh
```

### CMake options

- `STEERING_ONLY`: enable or disable steering-only mode (default: ON)
- `ROOT_VERSION`: set required ROOT version (default: 6.22.00)
- `CLANG_TIDY`: check the code with clang-tidy (default: OFF)

## Running

Available commands:

- `process <configName>` (run the fit, either locally or on the grid)
- `grid_merge <configName>` (merge the grid outputs after downloaded)
- `cleanup <configName>` (clean the local working files)
- `analyze <configName>` (read the limit from the result and make plots)
- `export_limits <configName>` (export the limits)

An example workflow would look like this:

1. Compute asymptotic limits locally.

   ```shell
   process Example_sys
   analyze Example_sys
   ```

2. Submit frequentist limits to the grid.

   ```shell
   process Example_sys -t grid -f
   <download the outputs>
   grid_merge Example_sys -f
   analyze Example_sys -f
   ```

   Add `-o <X>` e.g. `process Example_sys -t grid -f -o 1000` to submit additional jobs.
   Should be in multiples of the default number of jobs in the configuration.

3. Export the limits.

   ```shell
   export_limits Example_sys
   export_limits Example_sys -f
   ```
