# CMakeLists.txt for TNLimitFinder
cmake_minimum_required(VERSION 3.11 FATAL_ERROR)

# Add additional CMake macros and definitions
set(CMAKE_MODULE_PATH
    ${CMAKE_CURRENT_SOURCE_DIR}/cmake
)

include(ATLAS)
include(ParseVersion)

set(STEERING_ONLY ON CACHE STRING "Build steering only")
message(STATUS "TNLimitFinder built as steering only: ${STEERING_ONLY}")

# Set project
if(NOT STEERING_ONLY)
    project(TNLimitFinder VERSION ${TNLimitFinder_VERSION} LANGUAGES CXX)
else()
    project(TNLimitFinder VERSION ${TNLimitFinder_VERSION} LANGUAGES NONE)
endif()

include(Locations)

# Dependencies
include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}/dependencies
    ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/GSL/include
)

set(TNLimitFinder_DEPS_TEST ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/prll/tests)
if(NOT EXISTS "${TNLimitFinder_DEPS_TEST}" OR NOT IS_DIRECTORY "${TNLimitFinder_DEPS_TEST}")
    message(FATAL_ERROR "Submodules are not initialised!")
endif()

if(NOT STEERING_ONLY)
    # ROOT
    include(CompilerConfig)
    include(ROOT)

    # TNLimitFinder common definitions
    set(TNLimitFinder_LIB TNLimitFinder)
    set(Dependencies_LIB TNLimitFinderDependencies)

    # Configure
    configure_file(
        ${CMAKE_CURRENT_SOURCE_DIR}/src/Config.h.in
        ${CMAKE_CURRENT_BINARY_DIR}/src/Config.h
    )

    # Build dependencies
    add_subdirectory(dependencies)

    # Build library
    add_subdirectory(src)
    add_subdirectory(src/tasks)
endif()

# Install files
tn_install_binary(bin/analyze)
tn_install_binary(bin/cleanup)
tn_install_binary(bin/compile)
tn_install_binary(bin/export_limits)
tn_install_binary(bin/grid_merge)
tn_install_binary(bin/process)
tn_install_binary(bin/test_run)

tn_install_files(scripts/setup.sh)
tn_install_files(VERSION)
tn_install_scripts(scripts/common/*.sh FOLDER common)
tn_install_scripts(scripts/completions/* FOLDER completions)
tn_install_scripts(scripts/jobs/*.sh FOLDER jobs)
tn_install_scripts(scripts/processing/* FOLDER processing)
tn_install_scripts(scripts/setup/*.sh FOLDER setup)
tn_install_scripts(scripts/submission/* FOLDER submission)

# Dependencies: PRLL
execute_process(COMMAND make --silent
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/prll/)

tn_install_binary(dependencies/prll/prll_bfr)
tn_install_binary(dependencies/prll/prll_qer)
tn_install_scripts(dependencies/prll/prll.sh FOLDER setup)
