#
# Minimal environment configuration file setting up the installed project.
#

# Set up the ROOT installation:
export PYTHONPATH=/usr/local/lib

# Set up the TNLimitFinder installation:
export PATH=$PATH:/opt/TNLimitFinder/bin
echo "Configured minimal TNLimitFinder from: $TNLIMITFINDER"

# Set up the prompt:
export PS1='\[\033[01;35m\][bash]\[\033[01;31m\][\u TNLimitFinder-$TNLIMITFINDER_VERSION]\[\033[01;34m\]:\W >\[\033[00m\] ';
