FROM ntadej/root-ubuntu:6.20-py3-patched

# Set TERM
ENV TERM=xterm-256color

# Copy installation everything into the image.
COPY ci_install/ /

# Set up the environment setup scripts:
COPY docker/analysis_setup*.sh /root/

# Set up the message of the day:
COPY docker/motd /etc/

# Start the image with BASH by default, after having printed the message
# of the day.
CMD cat /etc/motd && /bin/bash
