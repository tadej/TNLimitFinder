#include <iostream>

#include "common/Errors.h"
#include "common/ToolHelpers.h"
#include "config/JobConfig.h"

static const char *USAGE =
    R"(TNLimitFinder jobs

Usage:
  tn_jobs <config>
  tn_jobs (-h | --help)
  tn_jobs --version

Options:
  -h --help            Show help screen.
  --version            Show version.
)";

int main(int argc, char **argv)
{
    std::string config;

    try {
        std::map<std::string, docopt::value> args
            = TNLimitFinder::parseArgv(USAGE, argc, argv);

        config = args["<config>"].asString();
    } catch (const std::invalid_argument &) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineParseError);
    } catch (...) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineRuntimeError);
    }

    // Read the config
    TNLimitFinder::JobConfig jobConfig(config);

    // Print the job count
    std::cout << jobConfig.njobs() << std::endl;

    return 0;
}
