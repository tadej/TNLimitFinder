#include <iostream>

#include <RooStats/HypoTestInverterResult.h>

#include "common/Errors.h"
#include "common/FileSystemHelpers.h"
#include "common/ToolHelpers.h"

static const char *USAGE =
    R"(TNLimitFinder range

Usage:
  tn_range <config> <input>
  tn_range (-h | --help)
  tn_range --version

Options:
  -h --help            Show help screen.
  --version            Show version.
)";

int main(int argc, char **argv)
{
    std::string config;
    std::string input;

    try {
        std::map<std::string, docopt::value> args
            = TNLimitFinder::parseArgv(USAGE, argc, argv);

        config = args["<config>"].asString();
        input = args["<input>"].asString();
    } catch (const std::invalid_argument &) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineParseError);
    } catch (...) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineRuntimeError);
    }

    // Open input result
    int status{};
    std::unique_ptr<TFile> inFile = TNLimitFinder::FS::openFile(input, &status);
    if (status != 0) {
        return status;
    }

    // Get the range
    double poimin{};
    double poimax{};
    // Read the asymptotic result
    std::unique_ptr<RooStats::HypoTestInverterResult> result{};
    result.reset(inFile->Get<RooStats::HypoTestInverterResult>("result_mu_signal"));

    double obs = result->UpperLimit();
    double exp = result->GetExpectedUpperLimit(0);

    poimin = std::min(obs, exp);
    poimin = std::min(poimin, result->GetExpectedUpperLimit(-2));
    poimin *= 0.5; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

    poimax = std::max(obs, exp);
    poimax = std::max(poimax, result->GetExpectedUpperLimit(2));
    poimax *= 1.25; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

    std::cout << poimin << "\t" << poimax << std::endl;

    return 0;
}
