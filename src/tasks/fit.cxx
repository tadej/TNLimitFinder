#include <iostream>

#include "common/Errors.h"
#include "common/FileSystemHelpers.h"
#include "common/TimeStampStreambuf.h"
#include "common/ToolHelpers.h"
#include "config/JobConfig.h"
#include "fit/HypoTestHelpers.h"

static const char *USAGE =
    R"(TNLimitFinder fit

Usage:
  tn_fit <config> <job> <input> <output> [--toys] [<min>] [<max>]
  tn_fit (-h | --help)
  tn_fit --version

Options:
  -h --help            Show help screen.
  --version            Show version.
  --toys               Run with toys.
)";

int main(int argc, char **argv)
{
    std::string config;
    int job{};
    std::string input;
    std::string output;
    std::string minVal;
    std::string maxVal;
    bool toys{false};
    try {
        std::map<std::string, docopt::value> args
            = TNLimitFinder::parseArgv(USAGE, argc, argv);

        config = args["<config>"].asString();
        job = static_cast<int>(args["<job>"].asLong());
        input = args["<input>"].asString();
        output = args["<output>"].asString();
        toys = args["--toys"].asBool();
        if (toys) {
            minVal = args["<min>"].asString();
            maxVal = args["<max>"].asString();
        }
    } catch (const std::invalid_argument &) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineParseError);
    } catch (...) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineRuntimeError);
    }

    TNLimitFinder::TimeStampStreambuf buffer(std::cout);

    std::cout << "Running configuration: " << config << std::endl;
    std::cout << "Job number: " << job << std::endl;
    std::cout << "Running toys: " << toys << std::endl;
    std::cout << "Input file: " << input << std::endl;
    if (toys && !minVal.empty() && !minVal.empty()) {
        std::cout << "Running in POI range from " << minVal << " to " << maxVal << std::endl;
    }
    std::cout << "Output file: " << output << std::endl;

    // Read the config
    TNLimitFinder::JobConfig jobConfig(config);
    RooStats::HypoTestInvOptions fitOptions;
    fitOptions.randomSeed = job;

    // Open input tree files
    int status{};
    std::unique_ptr<TFile> inFile = TNLimitFinder::FS::openFile(input, &status);
    if (status != 0) {
        return status;
    }

    // Create the output file
    std::unique_ptr<TFile> outFile = TNLimitFinder::FS::createFile(output, false, &status);
    if (status != 0) {
        return status;
    }

    // Get the workspace
    auto *workspace = inFile->Get<RooWorkspace>("combined");
    if (workspace == nullptr) {
        workspace = inFile->Get<RooWorkspace>("w");
        if (workspace == nullptr) {
            return TNLimitFinder::returnError(TNLimitFinder::IOError::InputWorkspaceNotFound);
        }
    }

    TNLimitFinder::prepare(workspace);

    // Run the fit
    double poimin{};
    double poimax{};
    std::unique_ptr<RooStats::HypoTestInverterResult> result{};
    if (toys && !minVal.empty() && !minVal.empty()) {
        // Running with toys
        poimin = std::stod(minVal);
        poimax = std::stod(maxVal);

        // Running the full frequentist fit
        result = TNLimitFinder::calculateLimit(jobConfig,
                                               fitOptions,
                                               workspace,
                                               toys,
                                               false,
                                               poimin,
                                               poimax);
    } else {
        // Running the quick asymptotic fit to estimate better ranges
        result = TNLimitFinder::calculateLimit(jobConfig,
                                               fitOptions,
                                               workspace,
                                               false,
                                               true);

        if (result != nullptr) {
            poimax = 1.10 * result->GetExpectedUpperLimit(2); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        } else {
            // TODO(tadej): error
            return 1;
        }

        // Running the full asymptotic fit
        result = TNLimitFinder::calculateLimit(jobConfig,
                                               fitOptions,
                                               workspace,
                                               toys,
                                               false,
                                               poimin,
                                               poimax);
    }

    outFile->Close();

    return 0;
}
