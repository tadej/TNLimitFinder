#include <iostream>

#include <RooStats/HypoTestInverterResult.h>

#include "common/Errors.h"
#include "common/FileSystemHelpers.h"
#include "common/ToolHelpers.h"

static const char *USAGE =
    R"(TNLimitFinder merge

Usage:
  tn_merge <output> <input>...
  tn_merge (-h | --help)
  tn_merge --version

Options:
  -h --help            Show help screen.
  --version            Show version.
)";

int main(int argc, char **argv)
{
    std::string output;
    std::vector<std::string> inputs;

    try {
        std::map<std::string, docopt::value> args
            = TNLimitFinder::parseArgv(USAGE, argc, argv);

        output = args["<output>"].asString();
        inputs = args["<input>"].asStringList();
    } catch (const std::invalid_argument &) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineParseError);
    } catch (...) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineRuntimeError);
    }

    std::cout << "Input files:" << std::endl;
    for (const std::string &f : inputs) {
        std::cout << "\t" << f << std::endl;
    }
    std::cout << "Output file: " << output << std::endl;

    // Memory management
    TDirectory::AddDirectory(false);

    // Create the output file
    int status{};
    std::unique_ptr<TFile> outFile = TNLimitFinder::FS::createFile(output, false, &status);
    if (status != 0) {
        return status;
    }

    // Merge the results
    RooStats::HypoTestInverterResult *result{};
    size_t count{};
    for (const std::string &f : inputs) {
        count++;

        std::unique_ptr<TFile> file = TNLimitFinder::FS::openFile(f, &status);
        if (status != 0) {
            return status;
        }

        std::cout << "File #" << count << std::endl;

        if (result == nullptr) {
            result = file->Get<RooStats::HypoTestInverterResult>("result_mu_signal");
        } else {
            auto other = std::unique_ptr<RooStats::HypoTestInverterResult>(file->Get<RooStats::HypoTestInverterResult>("result_mu_signal"));
            result->Add(*other);
        }

        file->Close();
    }

    outFile->cd();
    if (result != nullptr) {
        result->Write();
    }
    outFile->Close();

    return 0;
}
