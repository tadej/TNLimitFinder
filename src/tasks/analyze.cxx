#include <iostream>

#include "common/Errors.h"
#include "common/FileSystemHelpers.h"
#include "common/ToolHelpers.h"
#include "fit/HypoTestHelpers.h"

static const char *USAGE =
    R"(TNLimitFinder analyze

Usage:
  tn_analyze <input> <basename> <output> [--toys]
  tn_analyze (-h | --help)
  tn_analyze --version

Options:
  -h --help            Show help screen.
  --version            Show version.
  --toys               Run with toys.
)";

int main(int argc, char **argv)
{
    std::string input;
    std::string basename;
    std::string output;
    bool toys{};

    try {
        std::map<std::string, docopt::value> args
            = TNLimitFinder::parseArgv(USAGE, argc, argv);

        input = args["<input>"].asString();
        basename = args["<basename>"].asString();
        output = args["<output>"].asString();
        toys = args["--toys"].asBool();
    } catch (const std::invalid_argument &) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineParseError);
    } catch (...) {
        return TNLimitFinder::returnError(TNLimitFinder::TaskError::CommandLineRuntimeError);
    }

    std::cout << "Input folder: " << input << std::endl;
    std::cout << "Base name: " << basename << std::endl;
    std::cout << "Output folder: " << output << std::endl;
    std::cout << "Running toys: " << toys << std::endl;

    // Merge the results
    int status{};
    RooStats::HypoTestInverterResult *result{};
    size_t count{};
    for (const auto &f : TNLimitFinder::FS::findFiles(basename, input)) {
        count++;

        std::unique_ptr<TFile> file = TNLimitFinder::FS::openFile(f, &status);
        if (status != 0) {
            return status;
        }

        std::cout << "File #" << count << std::endl;

        if (result == nullptr) {
            result = file->Get<RooStats::HypoTestInverterResult>("result_mu_signal");
        } else {
            auto other = std::unique_ptr<RooStats::HypoTestInverterResult>(file->Get<RooStats::HypoTestInverterResult>("result_mu_signal"));
            result->Add(*other);
        }

        file->Close();
    }

    if (result != nullptr) {
        RooStats::HypoTestInvOptions fitOptions;
        fitOptions.enableDetailedOutput = false;
        fitOptions.resultFolder = output;

        if (toys) {
            std::cout << "Toys/point: " << result->GetResult(0)->GetNullDistribution()->GetSize() << std::endl;
        }
        TNLimitFinder::analyzeLimit(result, fitOptions, toys);
    } else {
        return 1;
    }

    return 0;
}
