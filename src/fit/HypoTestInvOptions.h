#ifndef TN_HYPOTESTINVOPTIONS_
#define TN_HYPOTESTINVOPTIONS_

#include <string>

namespace RooStats
{

struct HypoTestInvOptions {

    bool plotHypoTestResult = true;   // plot test statistic result at each point
    bool writeResult = true;          // write HypoTestInverterResult in a file
    std::string resultFolder;         // folder with results
    bool optimize = true;             // optimize evaluation of test statistic
    bool useVectorStore = true;       // convert data to use new roofit data store
    bool generateBinned = true;       // generate binned data sets
    bool noSystematics = false;       // force all systematics to be off (i.e. set all nuisance parameters as constat
                                      // to their nominal values)
    double nToysRatio = 2;            // ratio Ntoys S+b/ntoysB
    double maxPOI = -1;               // max value used of POI (in case of auto scan)
    bool useProof = false;            // use Proof Lite when using toys (for freq or hybrid)
    int nworkers = 0;                 // number of worker for ProofLite (default use all available cores)
    bool enableDetailedOutput = true; // enable detailed output with all fit information for each toys (output will be written in result file)
    bool rebuild = false;             // re-do extra toys for computing expected limits and rebuild test stat
                                      // distributions (N.B this requires much more CPU (factor is equivalent to nToyToRebuild)
    int nToyToRebuild = 100;          // number of toys used to rebuild // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    int rebuildParamValues = 0;       // = 0   do a profile of all the parameters on the B (alt snapshot) before performing a
                                      // rebuild operation (default)
                                      // = 1   use initial workspace parameters with B snapshot values
                                      // = 2   use all initial workspace parameters with B
                                      // Otherwise the rebuild will be performed using
    int initialFit = -1;              // do a first  fit to the model (-1 : default, 0 skip fit, 1 do always fit)
    int randomSeed = -1;              // random seed (if = -1: use default value, if = 0 always random )
                                      // NOTE: Proof uses automatically a random seed

    int nAsimovBins = 0; // number of bins in observables used for Asimov data sets (0 is the default and it is given by
                         // workspace, typically is 100)

    bool reuseAltToys = false; // reuse same toys for alternate hypothesis (if set one gets more stable bands)
    double confLevel = 0.95;   // confidence level value // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

    std::string minimizerType = "Minuit2"; // minimizer type (default is what is in ROOT::Math::MinimizerOptions::DefaultMinimizerType()
    std::string massValue = "";            // extra string to tag output file of result
    int printLevel = 0;                    // print level for debugging PL test statistics and calculators

    bool useNLLOffset = true; // use NLL offset when fitting (this increase stability of fits)
};

} // namespace RooStats

#endif // TN_HYPOTESTINVOPTIONS_
