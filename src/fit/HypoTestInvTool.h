#ifndef TN_HYPOTESTINVTOOL_
#define TN_HYPOTESTINVTOOL_

#include <string>

#include <RooStats/HypoTestInverterResult.h>

#include "fit/HypoTestInvOptions.h"

namespace RooStats
{

class HypoTestInvTool
{

public:
    explicit HypoTestInvTool(const HypoTestInvOptions &options);

    HypoTestInverterResult *RunInverter(RooWorkspace *workspace,
                                        const std::string &modelSBName,
                                        const std::string &modelBName,
                                        const std::string &dataName,
                                        int calculatorType,
                                        int testStatType,
                                        bool useCLs,
                                        int npoints,
                                        double poimin,
                                        double poimax,
                                        int ntoys,
                                        bool useNumberCounting = false,
                                        const std::string &nuisPriorName = "");

    void AnalyzeResult(HypoTestInverterResult *result,
                       int calculatorType);

    void SetParameter(const std::string &name,
                      const std::string &value);
    void SetParameter(const std::string &name,
                      bool value);
    void SetParameter(const std::string &name,
                      int value);
    void SetParameter(const std::string &name,
                      double value);

private:
    static double minCLs(RooStats::HypoTestInverterResult *result);
    static double maxCLs(RooStats::HypoTestInverterResult *result);

    const HypoTestInvOptions &mOptions;

    bool mPlotHypoTestResult{true};
    bool mWriteResult{false};
    bool mOptimize{true};
    bool mUseVectorStore{true};
    bool mGenerateBinned{true};
    bool mUseProof{false};
    bool mRebuild{false};
    bool mReuseAltToys{false};
    bool mEnableDetOutput{true};
    int mNWorkers{4};        // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    int mNToyToRebuild{100}; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    int mRebuildParamValues{0};
    int mPrintLevel{0};
    int mInitialFit{-1};
    int mRandomSeed{-1};
    double mNToysRatio{2};
    double mMaxPoi{-1};
    int mAsimovBins{0};
    std::string mMassValue;
    std::string mMinimizerType;
    std::string mResultFolder;
};

} // end namespace RooStats

#endif // TN_HYPOTESTINVTOOL_
