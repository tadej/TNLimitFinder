#ifndef TN_HYPOTESTHELPERS_
#define TN_HYPOTESTHELPERS_

#include <string>

#include "config/JobConfig.h"
#include "fit/HypoTestInvTool.h"

namespace TNLimitFinder
{

void prepare(RooWorkspace *workspace);

std::unique_ptr<RooStats::HypoTestInverterResult>
calculateLimit(const JobConfig &config,
               const RooStats::HypoTestInvOptions &options,
               RooWorkspace *workspace,
               bool toys,
               bool quick = false,
               double poimin = -1,
               double poimax = -1);

void analyzeLimit(RooStats::HypoTestInverterResult *result,
                  const RooStats::HypoTestInvOptions &options,
                  bool toys);

} // namespace TNLimitFinder

#endif // TN_HYPOTESTHELPERS_
