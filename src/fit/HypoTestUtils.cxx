#include <TMath.h>
#include <RooGaussian.h>
#include <RooRealSumPdf.h>
#include <RooRealVar.h>
#include <RooStats/HistFactory/PiecewiseInterpolation.h>

#include "fit/HypoTestUtils.h"

namespace TNLimitFinder
{

const RooStats::ModelConfig *getModelConfig(const RooWorkspace *workspace,
                                            const std::string &name)
{
    if (workspace == nullptr) {
        return nullptr;
    }

    const TObject *obj = workspace->obj(name.c_str());
    if (obj == nullptr) {
        return nullptr;
    }

    const auto *mc = dynamic_cast<const RooStats::ModelConfig *>(obj);
    if (mc == nullptr) {
        return nullptr;
    }

    return mc;
}

RooArgList getFloatParList(const RooAbsPdf &pdf, const RooArgSet &obsSet)
{
    RooArgList floatParList;

    const RooArgSet *pars = pdf.getParameters(obsSet);
    if (pars == nullptr) {
        return floatParList;
    }

    auto iter = std::unique_ptr<TIterator>(pars->createIterator());
    RooAbsArg *arg{};
    while ((arg = dynamic_cast<RooAbsArg *>(iter->Next())) != nullptr) {
        if (arg->InheritsFrom("RooRealVar") && !arg->isConstant()) {
            floatParList.add(*arg);
        }
    }

    return floatParList;
}

void activateBinnedLikelihood(RooWorkspace *workspace)
{
    RooFIter iter = workspace->components().fwdIterator();
    RooAbsArg *arg{};
    while ((arg = iter.next()) != nullptr) {
        if (arg->IsA() == RooRealSumPdf::Class()) {
            arg->setAttribute("BinnedLikelihood");
            std::cout << "Activating binned likelihood attribute for " << arg->GetName() << std::endl;
        }
    }
}

void resetAllErrors(RooWorkspace *workspace)
{
    const RooStats::ModelConfig *mc = getModelConfig(workspace);
    if (mc == nullptr) {
        return;
    }

    const RooArgSet *obsSet = mc->GetObservables();
    if (obsSet == nullptr) {
        return;
    }

    const RooAbsPdf *pdf = mc->GetPdf();
    if (pdf == nullptr) {
        return;
    }

    resetError(workspace, getFloatParList(*pdf, *obsSet));
}

void resetError(RooWorkspace *workspace,
                const RooArgList &parList,
                const RooArgList &vetoList)

{
    auto iter = std::unique_ptr<TIterator>(parList.createIterator());

    RooAbsArg *arg{};
    while ((arg = dynamic_cast<RooAbsArg *>(iter->Next())) != nullptr) {
        std::string name;
        if (arg->InheritsFrom("RooRealVar") && !arg->isConstant()) {
            name = arg->GetName();
        } else {
            continue;
        }

        if (vetoList.FindObject(name.c_str()) != nullptr) {
            continue;
        }

        RooRealVar *var = workspace->var(name.c_str());
        if (var == nullptr) {
            std::cerr << "Could not find variable: " << name
                      << " in workspace: " << workspace->GetName() << ": " << workspace
                      << std::endl;
            continue;
        }

        double val_hi{DBL_MAX};
        double val_low{DBL_MIN};
        double sigma{};
        bool resetRange{false};

        if (name.empty()) {
            std::cerr << "No Uncertainty Name provided" << std::endl;
            throw - 1;
        }

        // If it is a standard (gaussian) uncertainty
        if (name.find("alpha") != std::string::npos) {
            // Assume the values are +1, -1
            val_hi = 1.0;
            val_low = -1.0;
            sigma = 1.0;
            resetRange = true;
        }
        // If it is Lumi:
        else if (name == "Lumi") {
            // Get the Lumi's constraint term:
            auto *lumiConstr = dynamic_cast<RooGaussian *>(workspace->pdf("lumiConstraint"));
            if (lumiConstr == nullptr) {
                std::cerr << "Could not find wspace->pdf('lumiConstraint') "
                          << " in workspace: " << workspace->GetName() << ": " << workspace
                          << " when trying to reset error for parameter: Lumi"
                          << std::endl;
                continue;
            }
            // Get the uncertainty on the Lumi:
            const RooAbsReal *lumiSigma = dynamic_cast<RooAbsReal *>(lumiConstr->findServer(0));
            sigma = lumiSigma->getVal();

            const RooRealVar *nominalLumi = workspace->var("nominalLumi");
            double val_nom = nominalLumi->getVal();

            val_hi = val_nom + sigma;
            val_low = val_nom - sigma;
            resetRange = true;
        }
        // If it is a stat uncertainty (gamma)
        else if (name.find("gamma") != std::string::npos) {

            // Get the constraint and check its type:
            const RooAbsReal *constraint = dynamic_cast<RooAbsReal *>(workspace->obj((name + "_constraint").c_str()));
            std::string type;
            if (constraint != nullptr) {
                type = constraint->IsA()->GetName();
            }

            if (type.empty()) {
                std::cout << "Assuming parameter :" << name << ": is a ShapeFactor and so unconstrained" << std::endl;
                continue;
            }
            if (type == "RooGaussian") {
                const RooAbsReal *sigmaVar = dynamic_cast<RooAbsReal *>(workspace->obj((name + "_sigma").c_str()));
                sigma = sigmaVar->getVal();

                // Symmetrize shifts
                val_hi = 1 + sigma;
                val_low = 1 - sigma;
                resetRange = true;
            } else if (type == "RooPoisson") {
                const RooAbsReal *nom_gamma = dynamic_cast<RooAbsReal *>(workspace->obj(("nom_" + name).c_str()));
                double nom_gamma_val = nom_gamma->getVal();

                sigma = 1 / TMath::Sqrt(nom_gamma_val);
                val_hi = 1 + sigma;
                val_low = 1 - sigma;
                resetRange = true;
            } else {
                std::cerr << "Strange constraint type for Stat Uncertainties: " << type << std::endl;
                throw - 1;
            }

        } // End Stat Error
        else {
            // Some unknown uncertainty
            std::cout << "Couldn't identify type of uncertainty for parameter: " << name << ". Assuming a normalization factor." << std::endl
                      << "Setting uncertainty to 0.0001 before the fit for parameter: " << name << std::endl;
            sigma = 0.0001; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            val_low = var->getVal() - sigma;
            val_hi = var->getVal() + sigma;
            resetRange = false;
        }

        var->setError(abs(sigma));
        if (resetRange) {
            double minrange = var->getMin();
            double maxrange = var->getMax();
            double newmin = var->getVal() - 6. * sigma; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            double newmax = var->getVal() + 6. * sigma; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            if (minrange < newmin) {
                var->setMin(newmin);
            }
            if (newmax < maxrange) {
                var->setMax(newmax);
            }
        }

        std::cout << "Uncertainties on parameter: " << name
                  << " low: " << val_low
                  << " high: " << val_hi
                  << " sigma: " << sigma
                  << " min range: " << var->getMin()
                  << " max range: " << var->getMax()
                  << std::endl;

        // Done
    } // end loop
}

void resetAllValues(RooWorkspace *workspace)
{
    const RooStats::ModelConfig *mc = getModelConfig(workspace);
    if (mc == nullptr) {
        return;
    }

    const RooArgSet *obsSet = mc->GetObservables();
    if (obsSet == nullptr) {
        return;
    }

    const RooAbsPdf *pdf = mc->GetPdf();
    if (pdf == nullptr) {
        return;
    }

    resetValue(workspace, getFloatParList(*pdf, *obsSet));
}

void resetValue(RooWorkspace *workspace,
                const RooArgList &parList,
                const RooArgList &vetoList)

{
    auto iter = std::unique_ptr<TIterator>(parList.createIterator());

    RooAbsArg *arg{};
    while ((arg = dynamic_cast<RooAbsArg *>(iter->Next())) != nullptr) {
        std::string name;
        if (arg->InheritsFrom("RooRealVar") && !arg->isConstant()) {
            name = arg->GetName();
        } else {
            continue;
        }

        if (vetoList.FindObject(name.c_str()) != nullptr) {
            continue;
        }

        RooRealVar *var = workspace->var(name.c_str());
        if (var == nullptr) {
            std::cerr << "Could not find variable: " << name
                      << " in workspace: " << workspace->GetName() << ": " << workspace
                      << std::endl;
            continue;
        }

        // Initialize
        double valnom = 0.;

        if (name.empty()) {
            std::cerr << "No Uncertainty Name provided" << std::endl;
            throw - 1;
        }

        // If it is a standard (gaussian) uncertainty
        if (name.find("alpha") != std::string::npos) {
            valnom = 0.0;
        } else {
            valnom = 1.0;
        }

        var->setVal(valnom);
        // Done
    } // end loop
}

void resetAllNominalValues(RooWorkspace *workspace)
{
    const RooStats::ModelConfig *mc = getModelConfig(workspace);
    if (mc == nullptr) {
        return;
    }

    const RooArgSet *gobsSet = mc->GetGlobalObservables();
    if (gobsSet == nullptr) {
        return;
    }

    gobsSet->Print("v");

    resetNominalValue(workspace, *gobsSet);
}

void resetNominalValue(RooWorkspace *workspace,
                       const RooArgSet &globSet)
{
    auto iter = std::unique_ptr<TIterator>(globSet.createIterator());
    RooAbsArg *arg{};
    while ((arg = dynamic_cast<RooAbsArg *>(iter->Next())) != nullptr) {
        std::string name;
        if (arg->InheritsFrom("RooRealVar") && arg->isConstant()) {
            name = arg->GetName();
        } else {
            continue;
        }

        RooRealVar *var = workspace->var(name.c_str());
        if (var == nullptr) {
            std::cerr << "Could not find variable: " << name
                      << " in workspace: " << workspace->GetName() << ": " << workspace
                      << std::endl;
            continue;
        }

        // Initialize
        double valnom{};

        if (name.empty()) {
            std::cerr << "No Uncertainty Name provided" << std::endl;
            throw - 1;
        }
        // If it is Lumi or gamma:
        if (name == "nominalLumi" || name.find("gamma") != std::string::npos) {
            valnom = 1.0;
        }
        // If it is a standard (gaussian) uncertainty
        else if (name.find("nom") == 0) {
            valnom = 0.0;
        }
        var->setVal(valnom);

        std::cout << "Now resetting: " << name << " to " << valnom << std::endl;
        // Done
    } // end loop
}

} // namespace TNLimitFinder
