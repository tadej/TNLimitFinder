#ifndef TN_HYPOTESTUTILS_
#define TN_HYPOTESTUTILS_

#include <string>

#include <RooArgList.h>
#include <RooArgSet.h>
#include <RooWorkspace.h>
#include <RooStats/ModelConfig.h>

namespace TNLimitFinder
{

const RooStats::ModelConfig *getModelConfig(const RooWorkspace *workspace,
                                            const std::string &name = "ModelConfig");
RooArgList getFloatParList(const RooAbsPdf &pdf,
                           const RooArgSet &obsSet = RooArgSet());

void activateBinnedLikelihood(RooWorkspace *w);

void resetAllErrors(RooWorkspace *workspace);
void resetError(RooWorkspace *workspace,
                const RooArgList &parList,
                const RooArgList &vetoList = RooArgList());
void resetAllValues(RooWorkspace *workspace);
void resetValue(RooWorkspace *workspace,
                const RooArgList &parList,
                const RooArgList &vetoList = RooArgList());
void resetAllNominalValues(RooWorkspace *workspace);
void resetNominalValue(RooWorkspace *workspace,
                       const RooArgSet &globSet);

} // namespace TNLimitFinder

#endif // TN_HYPOTESTUTILS_
