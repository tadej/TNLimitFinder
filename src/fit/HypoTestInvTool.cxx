#include <libs/json.hpp>

#include <TCanvas.h>
#include <TSystem.h>

#include <RooGlobalFunc.h>
#include <RooRandom.h>

#include <Math/MinimizerOptions.h>

#include <RooStats/AsymptoticCalculator.h>
#include <RooStats/FrequentistCalculator.h>
#include <RooStats/HybridCalculator.h>
#include <RooStats/HypoTestInverter.h>
#include <RooStats/HypoTestInverterPlot.h>
#include <RooStats/HypoTestPlot.h>
#include <RooStats/MaxLikelihoodEstimateTestStat.h>
#include <RooStats/NumEventsTestStat.h>
#include <RooStats/ProfileLikelihoodTestStat.h>
#include <RooStats/RatioOfProfiledLikelihoodsTestStat.h>
#include <RooStats/SimpleLikelihoodRatioTestStat.h>
#include <RooStats/ToyMCSampler.h>

#include "fit/HypoTestInvTool.h"

namespace RooStats
{

HypoTestInvTool::HypoTestInvTool(const HypoTestInvOptions &options)
    : mOptions(options)
{
    // set parameters
    SetParameter("PlotHypoTestResult", options.plotHypoTestResult);
    SetParameter("WriteResult", options.writeResult);
    SetParameter("Optimize", options.optimize);
    SetParameter("UseVectorStore", options.useVectorStore);
    SetParameter("GenerateBinned", options.generateBinned);
    SetParameter("NToysRatio", options.nToysRatio);
    SetParameter("MaxPOI", options.maxPOI);
    SetParameter("UseProof", options.useProof);
    SetParameter("EnableDetailedOutput", options.enableDetailedOutput);
    SetParameter("NWorkers", options.nworkers);
    SetParameter("Rebuild", options.rebuild);
    SetParameter("ReuseAltToys", options.reuseAltToys);
    SetParameter("NToyToRebuild", options.nToyToRebuild);
    SetParameter("RebuildParamValues", options.rebuildParamValues);
    SetParameter("MassValue", options.massValue);
    SetParameter("MinimizerType", options.minimizerType);
    SetParameter("PrintLevel", options.printLevel);
    SetParameter("InitialFit", options.initialFit);
    SetParameter("ResultFolder", options.resultFolder);
    SetParameter("RandomSeed", options.randomSeed);
    SetParameter("AsimovBins", options.nAsimovBins);

    // enable offset for all roostats
    if (options.useNLLOffset) {
        RooStats::UseNLLOffset(true);
    }
}

// internal routine to run the inverter
HypoTestInverterResult *HypoTestInvTool::RunInverter(RooWorkspace *workspace,
                                                     const std::string &modelSBName,
                                                     const std::string &modelBName,
                                                     const std::string &dataName,
                                                     int type,
                                                     int testStatType,
                                                     bool useCLs,
                                                     int npoints,
                                                     double poimin,
                                                     double poimax,
                                                     int ntoys,
                                                     bool useNumberCounting,
                                                     const std::string &nuisPriorName)
{
    std::cout << "Running HypoTestInverter on the workspace " << workspace->GetName() << std::endl;

    // workspace->Print();

    RooAbsData *data = workspace->data(dataName.c_str());
    if (data == nullptr) {
        std::cerr << "Error: Non-existing data " << dataName << std::endl;
        return nullptr;
    }

    if (mUseVectorStore) {
        RooAbsData::setDefaultStorageType(RooAbsData::Vector);
        data->convertToVectorStore();
    }

    // get models from WS
    // get the modelConfig out of the file
    auto *bModel = dynamic_cast<ModelConfig *>(workspace->obj(modelBName.c_str()));
    auto *sbModel = dynamic_cast<ModelConfig *>(workspace->obj(modelSBName.c_str()));

    if (sbModel == nullptr) {
        std::cerr << "Error: Non-existing ModelConfig " << modelSBName << std::endl;
        return nullptr;
    }
    // check the model
    if (sbModel->GetPdf() == nullptr) {
        std::cerr << "Error: Model " << modelSBName << " has no pdf" << std::endl;
        return nullptr;
    }
    if (sbModel->GetParametersOfInterest() == nullptr) {
        std::cerr << "Error: Model " << modelSBName << " has no poi" << std::endl;
        return nullptr;
    }
    if (sbModel->GetObservables() == nullptr) {
        std::cerr << "Error: Model " << modelSBName << " has no observables" << std::endl;
        return nullptr;
    }
    if (sbModel->GetSnapshot() == nullptr) {
        std::cout << "Model " << modelSBName << " has no snapshot  - make one using model poi" << std::endl;
        auto *var = dynamic_cast<RooRealVar *>(sbModel->GetParametersOfInterest()->first());
        if (var != nullptr) {
            std::cout << "Setting POI to 1.0" << std::endl;
            var->setVal(1.0);
        }
        sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
    }

    // case of no systematics
    // remove nuisance parameters from model
    if (mOptions.noSystematics) {
        const RooArgSet *nuisPar = sbModel->GetNuisanceParameters();
        if (nuisPar != nullptr && nuisPar->getSize() > 0) {
            std::cout << "Switch off all systematics by setting them constant to their initial values" << std::endl;
            SetAllConstant(*nuisPar);
        }
        if (bModel != nullptr) {
            const RooArgSet *bnuisPar = bModel->GetNuisanceParameters();
            if (bnuisPar != nullptr) {
                SetAllConstant(*bnuisPar);
            }
        }
    }

    if (bModel == nullptr || bModel == sbModel) {
        std::cout << "The background model " << modelBName << " does not exist" << std::endl
                  << "Copy it from ModelConfig " << modelSBName << " and set POI to zero" << std::endl;
        bModel = dynamic_cast<ModelConfig *>(sbModel->Clone());
        bModel->SetName(TString(modelSBName) + TString("_with_poi_0"));
        auto *var = dynamic_cast<RooRealVar *>(bModel->GetParametersOfInterest()->first());
        if (var == nullptr) {
            return nullptr;
        }
        double oldval = var->getVal();
        var->setVal(0);
        bModel->SetSnapshot(RooArgSet(*var));
        var->setVal(oldval);
    } else {
        if (bModel->GetSnapshot() == nullptr) {
            std::cout << "Model " << modelBName << " has no snapshot  - make one using model poi and 0 values" << std::endl;
            auto *var = dynamic_cast<RooRealVar *>(bModel->GetParametersOfInterest()->first());
            if (var != nullptr) {
                double oldval = var->getVal();
                var->setVal(0);
                bModel->SetSnapshot(RooArgSet(*var));
                var->setVal(oldval);
            } else {
                std::cerr << "Error: Model " << modelBName << " has no valid poi" << std::endl;
                return nullptr;
            }
        }
    }

    // check model  has global observables when there are nuisance pdf
    // for the hybrid case the globals are not needed
    if (type != 1) {
        bool hasNuisParam = (sbModel->GetNuisanceParameters() != nullptr && sbModel->GetNuisanceParameters()->getSize() > 0);
        bool hasGlobalObs = (sbModel->GetGlobalObservables() != nullptr && sbModel->GetGlobalObservables()->getSize() > 0);
        if (hasNuisParam && !hasGlobalObs) {
            // try to see if model has nuisance parameters first
            auto constrPdf = std::unique_ptr<RooAbsPdf>(MakeNuisancePdf(*sbModel, "nuisanceConstraintPdf_sbmodel"));
            if (constrPdf != nullptr) {
                std::cerr << "Warning: Model " << sbModel->GetName() << " has nuisance parameters but no global observables associated" << std::endl
                          << "The effect of the nuisance parameters will not be treated correctly" << std::endl;
            }
        }
    }

    // save all initial parameters of the model including the global observables
    RooArgSet initialParameters;
    auto allParams = std::unique_ptr<RooArgSet>(sbModel->GetPdf()->getParameters(*data));
    allParams->snapshot(initialParameters);
    allParams.reset();

    // run first a data fit
    const RooArgSet *poiSet = sbModel->GetParametersOfInterest();
    auto *poi = dynamic_cast<RooRealVar *>(poiSet->first());

    std::cout << "POI initial value: " << poi->GetName() << " = " << poi->getVal() << std::endl;

    // fit the data first (need to use constraint)
    TStopwatch tw;

    bool doFit = mInitialFit != 0;
    if (testStatType == 0 && mInitialFit == -1) {
        doFit = false; // case of LEP test statistic
    }
    if (type == 3 && mInitialFit == -1) {
        doFit = false; // case of Asymptoticcalculator with nominal Asimov
    }

    if (mMinimizerType.empty()) {
        mMinimizerType = ROOT::Math::MinimizerOptions::DefaultMinimizerType();
    } else {
        ROOT::Math::MinimizerOptions::SetDefaultMinimizer(mMinimizerType.c_str());
    }

    std::cout << "Using " << ROOT::Math::MinimizerOptions::DefaultMinimizerType() << " as minimizer for computing the test statistic" << std::endl;

    double poihat{};
    if (doFit) {
        // do the fit : By doing a fit the POI snapshot (for S+B)  is set to the fit value
        // and the nuisance parameters nominal values will be set to the fit value.
        // This is relevant when using LEP test statistics

        std::cout << "Doing a first fit to the observed data" << std::endl;
        RooArgSet constrainParams;
        if (sbModel->GetNuisanceParameters() != nullptr) {
            constrainParams.add(*sbModel->GetNuisanceParameters());
        }
        RemoveConstantParameters(&constrainParams);
        tw.Start();
        auto fitres = std::unique_ptr<RooFitResult>(sbModel->GetPdf()->fitTo(
            *data,
            RooFit::InitialHesse(false),
            RooFit::Hesse(false),
            RooFit::Minimizer(mMinimizerType.c_str(), "Migrad"),
            RooFit::Strategy(0),
            RooFit::PrintLevel(mPrintLevel),
            RooFit::Constrain(constrainParams),
            RooFit::Save(true)
            // RooFit::Offset(IsNLLOffset())
            ));
        if (fitres->status() != 0) {
            std::cerr << "Warning: Fit to the model failed - try with strategy 1 and perform first an Hesse computation" << std::endl;
            fitres.reset(sbModel->GetPdf()->fitTo(
                *data,
                RooFit::InitialHesse(true),
                RooFit::Hesse(false),
                RooFit::Minimizer(mMinimizerType.c_str(), "Migrad"),
                RooFit::Strategy(1),
                RooFit::PrintLevel(mPrintLevel),
                RooFit::Constrain(constrainParams),
                RooFit::Save(true)
                // RooFit::Offset(IsNLLOffset())
                ));
        }
        if (fitres->status() != 0) {
            std::cerr << "Warning: Fit still failed - continue anyway....." << std::endl;
        }

        poihat = poi->getVal();
        std::cout << "Best Fit value : " << poi->GetName() << " = " << poihat << " +/- " << poi->getError() << std::endl;
        std::cout << "Time used for fitting : ";
        tw.Print();

        // save best fit value in the poi snapshot
        sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
        std::cout << "Snapshot of S+B Model " << sbModel->GetName() << " is set to the best fit value" << std::endl;
    }

    poi->setVal(poihat);

    // print a message in case of LEP test statistics because it affects result by doing or not doing a fit
    if (testStatType == 0) {
        if (!doFit) {
            std::cout << "Using LEP test statistic - an initial fit is not done and the TS will use "
                         "the nuisances at the model value"
                      << std::endl;
        } else {
            std::cout << "Using LEP test statistic - an initial fit has been done and the TS will use "
                         "the nuisances at the best fit value"
                      << std::endl;
        }
    }

    // build test statistics and hypotest calculators for running the inverter
    SimpleLikelihoodRatioTestStat slrts(*sbModel->GetPdf(), *bModel->GetPdf());

    // null parameters must includes snapshot of poi plus the nuisance values
    RooArgSet nullParams(*sbModel->GetSnapshot());
    if (sbModel->GetNuisanceParameters() != nullptr) {
        nullParams.add(*sbModel->GetNuisanceParameters());
    }
    if (sbModel->GetSnapshot() != nullptr) {
        slrts.SetNullParameters(nullParams);
    }
    RooArgSet altParams(*bModel->GetSnapshot());
    if (bModel->GetNuisanceParameters() != nullptr) {
        altParams.add(*bModel->GetNuisanceParameters());
    }
    if (bModel->GetSnapshot() != nullptr) {
        slrts.SetAltParameters(altParams);
    }
    if (mEnableDetOutput) {
        slrts.EnableDetailedOutput();
    }

    // ratio of profile likelihood - need to pass snapshot for the alt
    RatioOfProfiledLikelihoodsTestStat ropl(*sbModel->GetPdf(), *bModel->GetPdf(), bModel->GetSnapshot());
    ropl.SetSubtractMLE(false);
    if (testStatType == 11) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        ropl.SetSubtractMLE(true);
    }
    ropl.SetPrintLevel(mPrintLevel);
    ropl.SetMinimizer(mMinimizerType.c_str());
    if (mEnableDetOutput) {
        ropl.EnableDetailedOutput();
    }

    ProfileLikelihoodTestStat profll(*sbModel->GetPdf());
    if (testStatType == 3) {
        profll.SetOneSided(true);
    }
    if (testStatType == 4) {
        profll.SetSigned(true);
    }
    profll.SetMinimizer(mMinimizerType.c_str());
    profll.SetPrintLevel(mPrintLevel);
    if (mEnableDetOutput) {
        profll.EnableDetailedOutput();
    }

    profll.SetReuseNLL(mOptimize);
    slrts.SetReuseNLL(mOptimize);
    ropl.SetReuseNLL(mOptimize);

    if (mOptimize) {
        profll.SetStrategy(0);
        ropl.SetStrategy(0);
        // ROOT::Math::MinimizerOptions::SetDefaultStrategy(0);
    }

    if (mMaxPoi > 0) {
        poi->setMax(mMaxPoi); // increase limit
    }

    MaxLikelihoodEstimateTestStat maxll(*sbModel->GetPdf(), *poi);
    NumEventsTestStat nevtts;

    AsymptoticCalculator::SetPrintLevel(mPrintLevel);

    // create the HypoTest calculator class
    std::unique_ptr<HypoTestCalculatorGeneric> hc{};
    if (type == 0) {
        hc = std::make_unique<FrequentistCalculator>(*data, *bModel, *sbModel);
    } else if (type == 1) {
        hc = std::make_unique<HybridCalculator>(*data, *bModel, *sbModel);
    } else if (type == 2) {
        hc = std::make_unique<AsymptoticCalculator>(*data, *bModel, *sbModel, false);
    } else if (type == 3) {
        hc = std::make_unique<AsymptoticCalculator>(*data, *bModel, *sbModel,
                                                    true); // for using Asimov data generated with nominal values
    } else {
        std::cerr << "Error: Invalid - calculator type = " << type
                  << ", supported values are only :\n\t\t\t 0 "
                     "(Frequentist) , 1 (Hybrid) , 2 (Asymptotic) "
                  << std::endl;
        return nullptr;
    }

    // set the test statistic
    TestStatistic *testStat{};
    if (testStatType == 0) {
        testStat = &slrts;
    }
    if (testStatType == 1 || testStatType == 11) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        testStat = &ropl;
    }
    if (testStatType == 2 || testStatType == 3 || testStatType == 4) {
        testStat = &profll;
    }
    if (testStatType == 5) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        testStat = &maxll;
    }
    if (testStatType == 6) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        testStat = &nevtts;
    }

    if (testStat == nullptr) {
        std::cerr << "Error: Invalid - test statistic type = " << testStatType
                  << ", supported values are only :\n\t\t\t 0 (SLR) "
                     ", 1 (Tevatron) , 2 (PLR), 3 (PLR1), 4(MLE)"
                  << std::endl;
        return nullptr;
    }

    auto *toymcs = dynamic_cast<ToyMCSampler *>(hc->GetTestStatSampler());
    if (toymcs != nullptr && (type == 0 || type == 1)) {
        // look if pdf is number counting or extended
        if (sbModel->GetPdf()->canBeExtended()) {
            if (useNumberCounting) {
                std::cerr << "Warning: Pdf is extended: but number counting flag is set: ignore it" << std::endl;
            }
        } else {
            // for not extended pdf
            if (!useNumberCounting) {
                int nEvents = data->numEntries();
                std::cout << "Pdf is not extended: number of events to generate taken  from observed data set is " << nEvents << std::endl;
                toymcs->SetNEventsPerToy(nEvents);
            } else {
                std::cout << "Using a number counting pdf" << std::endl;
                toymcs->SetNEventsPerToy(1);
            }
        }

        toymcs->SetTestStatistic(testStat);

        if (data->isWeighted() && !mGenerateBinned) {
            std::cout << "Data set is weighted, nentries = " << data->numEntries()
                      << " and sum of weights = " << data->sumEntries()
                      << " but toy generation is unbinned - it would be faster to set mGenerateBinned to true"
                      << std::endl;
        }
        toymcs->SetGenerateBinned(mGenerateBinned);

        toymcs->SetUseMultiGen(mOptimize);

        if (mGenerateBinned && sbModel->GetObservables()->getSize() > 2) {
            std::cerr << "Warning: Generate binned is activated but the number of observable is "
                      << sbModel->GetObservables()->getSize()
                      << " Too much memory could be needed for allocating all the bins" << std::endl;
        }

        // set the random seed if needed
        if (mRandomSeed >= 0) {
            RooRandom::randomGenerator()->SetSeed(mRandomSeed);
        }
    }

    // specify if need to re-use same toys
    if (mReuseAltToys) {
        hc->UseSameAltToys();
    }

    if (type == 1) {
        auto *hhc = dynamic_cast<HybridCalculator *>(hc.get());
        if (hhc == nullptr) {
            // TODO(tadej)
            // internal error
            return nullptr;
        }

        hhc->SetToys(ntoys, static_cast<int>(ntoys / mNToysRatio)); // can use less ntoys for b hypothesis

        // remove global observables from ModelConfig (this is probably not needed anymore in 5.32)
        bModel->SetGlobalObservables(RooArgSet());
        sbModel->SetGlobalObservables(RooArgSet());

        // check for nuisance prior pdf in case of nuisance parameters
        if (bModel->GetNuisanceParameters() != nullptr || sbModel->GetNuisanceParameters() != nullptr) {

            // fix for using multigen (does not work in this case)
            toymcs->SetUseMultiGen(false);
            ToyMCSampler::SetAlwaysUseMultiGen(false);

            RooAbsPdf *nuisPdf{};
            if (!nuisPriorName.empty()) {
                nuisPdf = workspace->pdf(nuisPriorName.c_str());
            }
            // use prior defined first in bModel (then in SbModel)
            if (nuisPdf == nullptr) {
                std::cout << "No nuisance pdf given for the HybridCalculator - try to deduce  pdf from the model" << std::endl;
                if (bModel->GetPdf() != nullptr && bModel->GetObservables() != nullptr) {
                    nuisPdf = MakeNuisancePdf(*bModel, "nuisancePdf_bmodel");
                } else {
                    nuisPdf = MakeNuisancePdf(*sbModel, "nuisancePdf_sbmodel");
                }
            }
            if (nuisPdf == nullptr) {
                if (bModel->GetPriorPdf() != nullptr) {
                    nuisPdf = bModel->GetPriorPdf();
                    std::cout << "No nuisance pdf given - try to use " << nuisPdf->GetName() << " that is defined as a prior pdf in the B model" << std::endl;
                } else {
                    std::cerr << "Error: Cannot run Hybrid calculator because no prior on the nuisance "
                                 "parameter is specified or can be derived"
                              << std::endl;
                    return nullptr;
                }
            }
            if (nuisPdf == nullptr) {
                // TODO(tadej)
                // internal error
                return nullptr;
            }
            std::cout << "Using as nuisance Pdf ... " << std::endl;
            nuisPdf->Print();

            const RooArgSet *nuisParams = bModel->GetNuisanceParameters() != nullptr ? bModel->GetNuisanceParameters() : sbModel->GetNuisanceParameters();
            auto np = std::unique_ptr<RooArgSet>(nuisPdf->getObservables(*nuisParams));
            if (np->getSize() == 0) {
                std::cerr << "Warning: Prior nuisance does not depend on nuisance parameters. They will be smeared in their full range" << std::endl;
            }

            hhc->ForcePriorNuisanceAlt(*nuisPdf);
            hhc->ForcePriorNuisanceNull(*nuisPdf);
        }
    } else if (type == 2 || type == 3) {
        if (testStatType == 3) {
            dynamic_cast<AsymptoticCalculator *>(hc.get())->SetOneSided(true);
        }
        if (testStatType != 2 && testStatType != 3) {
            std::cerr << "Warning: Only the PL test statistic can be used with AsymptoticCalculator - use by default a two-sided PL" << std::endl;
        }
    } else if (type == 0) {
        dynamic_cast<FrequentistCalculator *>(hc.get())->SetToys(ntoys, static_cast<int>(ntoys / mNToysRatio));
        // store also the fit information for each poi point used by calculator based on toys
        if (mEnableDetOutput) {
            dynamic_cast<FrequentistCalculator *>(hc.get())->StoreFitInfo(true);
        }
    } else if (type == 1) {
        dynamic_cast<HybridCalculator *>(hc.get())->SetToys(ntoys, static_cast<int>(ntoys / mNToysRatio));
        // store also the fit information for each poi point used by calculator based on toys
        // if (mEnableDetOutput) ((HybridCalculator*) hc)->StoreFitInfo(true);
    }

    // Get the result
    RooMsgService::instance().getStream(1).removeTopic(RooFit::NumIntegration);

    HypoTestInverter calc(*hc);
    calc.SetConfidenceLevel(mOptions.confLevel);

    calc.UseCLs(useCLs);
    calc.SetVerbose(1);

    // can speed up using proof-lite
    if (mUseProof) {
        ProofConfig pc(*workspace, mNWorkers, "", kFALSE);
        toymcs->SetProofConfig(&pc); // enable proof
    }

    // get models from WS
    sbModel = dynamic_cast<ModelConfig *>(workspace->obj(modelSBName.c_str()));

    if (sbModel == nullptr) {
        std::cerr << "Error: Non-existing ModelConfig " << modelSBName << std::endl;
        return nullptr;
    }
    // check the model
    if (sbModel->GetPdf() == nullptr) {
        std::cerr << "Error: Model " << modelSBName << " has no pdf" << std::endl;
        return nullptr;
    }
    if (sbModel->GetParametersOfInterest() == nullptr) {
        std::cerr << "Error: Model " << modelSBName << " has no poi" << std::endl;
        return nullptr;
    }
    if (sbModel->GetObservables() == nullptr) {
        std::cerr << "Error: Model " << modelSBName << " has no observables" << std::endl;
        return nullptr;
    }
    if (sbModel->GetSnapshot() == nullptr) {
        std::cout << "Model " << modelSBName << " has no snapshot  - make one using model poi" << std::endl;
        sbModel->SetSnapshot(*sbModel->GetParametersOfInterest());
    }

    poi = dynamic_cast<RooRealVar *>(sbModel->GetParametersOfInterest()->first());
    poihat = poi->getVal();

    if (npoints > 0) {
        if (poimin > poimax) {
            // if no min/max given scan between MLE and +4 sigma
            if (poimin > poihat) {
                poimin = poihat - 4 * poi->getError();
            }
            if (poimin < 0) {
                poimin = 0;
            }
            poimax = (poihat + 20 * poi->getErrorHi()); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        }
        std::cout << "Doing a fixed scan in interval : " << poimin << " , " << poimax << std::endl;
        calc.SetFixedScan(npoints, poimin, poimax);
    } else {
        std::cout << "Doing an  automatic scan  in interval : " << poi->getMin() << " , " << poi->getMax() << std::endl;
    }

    tw.Start();
    auto r = std::unique_ptr<HypoTestInverterResult>(calc.GetInterval());
    std::cout << "Time to perform limit scan \n";
    tw.Print();

    if (mRebuild) {

        std::cout << "\n***************************************************************\n";
        std::cout << "Rebuild the upper limit distribution by re-generating new set of pseudo-experiment and re-compute "
                     "for each of them a new upper limit\n\n";

        allParams.reset(sbModel->GetPdf()->getParameters(*data));

        // define on which value of nuisance parameters to do the rebuild
        // default is best fit value for bmodel snapshot

        if (mRebuildParamValues != 0) {
            // set all parameters to their initial workspace values
            *allParams = initialParameters;
        }
        if (mRebuildParamValues == 0 || mRebuildParamValues == 1) {
            RooArgSet constrainParams;
            if (sbModel->GetNuisanceParameters() != nullptr) {
                constrainParams.add(*sbModel->GetNuisanceParameters());
            }
            RemoveConstantParameters(&constrainParams);

            const RooArgSet *poiModel = sbModel->GetParametersOfInterest();
            bModel->LoadSnapshot();

            // do a profile using the B model snapshot
            if (mRebuildParamValues == 0) {
                SetAllConstant(*poiModel, true);

                // TODO(tadej): leak?
                sbModel->GetPdf()->fitTo(*data, RooFit::InitialHesse(false), RooFit::Hesse(false),
                                         RooFit::Minimizer(mMinimizerType.c_str(), "Migrad"), RooFit::Strategy(0), RooFit::PrintLevel(mPrintLevel),
                                         RooFit::Constrain(constrainParams), RooFit::Offset(IsNLLOffset()));

                std::cout << "Rebuild using fitted parameter value for B-model snapshot" << std::endl;
                constrainParams.Print("v");

                SetAllConstant(*poiModel, false);
            }
        }
        std::cout << "Initial parameters used for rebuilding:" << std::endl;
        PrintListContent(*allParams, std::cout);
        allParams.reset();

        HypoTestInverter::SetCloseProof(true);
        tw.Start();
        SamplingDistribution *limDist = calc.GetUpperLimitDistribution(true, mNToyToRebuild);
        std::cout << "Time to rebuild distributions: ";
        tw.Print();

        if (limDist != nullptr) {
            std::cout << "Expected limits after rebuild distribution " << std::endl;
            std::cout << "expected upper limit  (median of limit distribution) " << limDist->InverseCDF(0.5) << std::endl; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            std::cout << "expected -1 sig limit (0.16% quantile of limit dist) "
                      << limDist->InverseCDF(ROOT::Math::normal_cdf(-1)) << std::endl;
            std::cout << "expected +1 sig limit (0.84% quantile of limit dist) "
                      << limDist->InverseCDF(ROOT::Math::normal_cdf(1)) << std::endl;
            std::cout << "expected -2 sig limit (.025% quantile of limit dist) "
                      << limDist->InverseCDF(ROOT::Math::normal_cdf(-2)) << std::endl;
            std::cout << "expected +2 sig limit (.975% quantile of limit dist) "
                      << limDist->InverseCDF(ROOT::Math::normal_cdf(2)) << std::endl;

            // Plot the upper limit distribution
            SamplingDistPlot limPlot((mNToyToRebuild < 200) ? 50 : 100); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            limPlot.AddSamplingDistribution(limDist);
            limPlot.GetTH1F()->SetStats(true); // display statistics
            limPlot.SetLineColor(kBlue);
            new TCanvas("limPlot", "Upper Limit Distribution");
            limPlot.Draw();

            /// save result in a file
            limDist->SetName("RULDist");
            limDist->Write();

            // update r to a new updated result object containing the rebuilt expected p-values distributions
            // (it will not recompute the expected limit)
            r.reset(calc.GetInterval());

        } else {
            std::cerr << "Error: Failed to re-build distributions" << std::endl;
        }
    }

    return r.release();
}

void HypoTestInvTool::AnalyzeResult(HypoTestInverterResult *r,
                                    int calculatorType)
{

    // analyze result produced by the inverter, optionally save it in a file

    double lowerLimit = 0;
    double llError = 0;
#if defined ROOT_SVN_VERSION && ROOT_SVN_VERSION >= 44126
    if (r->IsTwoSided()) {
        lowerLimit = r->LowerLimit();
        llError = r->LowerLimitEstimatedError();
    }
#else
    lowerLimit = r->LowerLimit();
    llError = r->LowerLimitEstimatedError();
#endif

    double upperLimit = r->UpperLimit();
    double ulError = r->UpperLimitEstimatedError();

    // std::cout << "DEBUG : [ " << lowerLimit << " , " << upperLimit << "  ] " << std::endl;

    if (lowerLimit < upperLimit * (1. - 1.E-4) && lowerLimit != 0) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
        std::cout << "The computed lower limit is: " << lowerLimit << " +/- " << llError << std::endl;
    }
    std::cout << "The computed upper limit is: " << upperLimit << " +/- " << ulError << std::endl;

    // compute expected limit
    std::cout << "Expected upper limits, using the B (alternate) model : " << std::endl;
    std::cout << " expected limit (-2 sig) " << r->GetExpectedUpperLimit(-2) << std::endl;
    std::cout << " expected limit (-1 sig) " << r->GetExpectedUpperLimit(-1) << std::endl;
    std::cout << " expected limit (median) " << r->GetExpectedUpperLimit(0) << std::endl;
    std::cout << " expected limit (+1 sig) " << r->GetExpectedUpperLimit(1) << std::endl;
    std::cout << " expected limit (+2 sig) " << r->GetExpectedUpperLimit(2) << std::endl;

    // detailed output
    if (mEnableDetOutput) {
        mWriteResult = true;
        std::cout << "Detailed output will be written in output result file" << std::endl;
    } else {
        mWriteResult = false;
    }

    // write result in a file
    if (r != nullptr && mWriteResult) {
        std::string resultName = r->GetName();
        if (r->ArraySize() > 0 && minCLs(r) > 0.05) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            std::cerr << "No CLs value below threshold of 0.05 found - upper limit scan most likely failed." << std::endl
                      << "Will store result only for debugging purposes - do not use it in contour plots!" << std::endl;
            resultName.append("_debug");
        } else if (r->ArraySize() > 0 && maxCLs(r) < 0.05) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            std::cerr << "No CLs value above threshold of 0.05 found - upper limit scan most likely failed." << std::endl
                      << "Will store result only for debugging purposes - do not use it in contour plots!" << std::endl;
            if (!std::isnan(r->GetResult(0)->CLs())) {
                std::cerr << "Try rescan in range (0, " << r->GetXValue(0) << ")" << std::endl;
            }
            resultName.append("_debug");
        } else if (r->ArraySize() == 0) {
            std::cerr << "All fits seem to have failed - cannot compute upper limit!" << std::endl;
            resultName.append("_error");
        }

        r->SetName(resultName.c_str());
        r->Write();
        std::cout << "HypoTestInverterResult has been written in the file as " << r->GetName() << std::endl;
    }

    // plot the result ( p values vs scan points)
    std::string typeName;
    std::string typeNameShort;
    if (calculatorType == 0) {
        typeName = "Frequentist";
        typeNameShort = "freq";
    } else if (calculatorType == 1) {
        typeName = "Hybrid";
        typeNameShort = "hybrid";
    } else if (calculatorType == 2 || calculatorType == 3) {
        typeName = "Asymptotic";
        typeNameShort = "asym";
        mPlotHypoTestResult = false;
    }

    if (!mResultFolder.empty()) {
        nlohmann::json resultJson = {
            {"obs", upperLimit},
            {"exp", {r->GetExpectedUpperLimit(-2), r->GetExpectedUpperLimit(-1), r->GetExpectedUpperLimit(0), r->GetExpectedUpperLimit(1), r->GetExpectedUpperLimit(2)}}};

        // Write JSON to the output file
        std::string outputFile = mResultFolder;
        outputFile.append("/limit_" + typeNameShort + ".json");
        std::ofstream output(outputFile);
        output << resultJson << std::endl;
        output.close();
    }

    const char *resultName = r->GetName();
    std::string plotTitle = typeName;
    plotTitle.append(" CL Scan for workspace ").append(resultName);
    auto plot = std::make_unique<HypoTestInverterPlot>("HTI_Result_Plot", plotTitle.c_str(), r);
    // plot in a new canvas with style
    auto c1 = std::make_unique<TCanvas>((typeName + "_Scan").c_str());
    c1->SetLogy(0);
    plot->Draw("CLb 2CL"); // plot all and Clb
    if (mWriteResult) {
        c1->Write();
    }
    if (!mResultFolder.empty()) {
        std::string plotName = mResultFolder + "/scan_" + typeNameShort;
        c1->SaveAs((plotName + ".eps").c_str());
        c1->SaveAs((plotName + ".pdf").c_str());
    }

    // plot test statistics distributions for the two hypothesis
    if (mPlotHypoTestResult) {
        auto c2 = std::make_unique<TCanvas>((typeName + "_Distributions").c_str());
        const int nEntries = r->ArraySize();
        if (nEntries > 1) {
            int ny = TMath::CeilNint(TMath::Sqrt(nEntries));
            int nx = TMath::CeilNint(double(nEntries) / ny);
            c2->Divide(nx, ny);
        }
        for (int i = 0; i < nEntries; i++) {
            if (nEntries > 1) {
                c2->cd(i + 1);
            }
            SamplingDistPlot *pl = plot->MakeTestStatPlot(i);
            pl->SetLogYaxis(true);
            pl->Draw();
        }
        if (mWriteResult) {
            c2->Write();
        }
        if (!mResultFolder.empty()) {
            std::string plotName = mResultFolder + "/distributions_" + typeNameShort;
            c2->SaveAs((plotName + ".eps").c_str());
            c2->SaveAs((plotName + ".pdf").c_str());
        }
    }
}

double HypoTestInvTool::minCLs(RooStats::HypoTestInverterResult *result)
{
    double CLs = 1.;
    for (int i = 0; i < result->ArraySize(); i++) {
        if (result->GetResult(i)->CLs() < CLs) {
            CLs = result->GetResult(i)->CLs();
        }
    }
    return CLs;
}

double HypoTestInvTool::maxCLs(RooStats::HypoTestInverterResult *result)
{
    double CLs = 1.;
    for (int i = 0; i < result->ArraySize(); i++) {
        if (result->GetResult(i)->CLs() > CLs) {
            CLs = result->GetResult(i)->CLs();
        }
    }
    return CLs;
}

void HypoTestInvTool::SetParameter(const std::string &name,
                                   bool value)
{
    // set boolean parameters
    if (name == "PlotHypoTestResult") {
        mPlotHypoTestResult = value;
        return;
    }
    if (name == "WriteResult") {
        mWriteResult = value;
        return;
    }
    if (name == "Optimize") {
        mOptimize = value;
        return;
    }
    if (name == "UseVectorStore") {
        mUseVectorStore = value;
        return;
    }
    if (name == "GenerateBinned") {
        mGenerateBinned = value;
        return;
    }
    if (name == "UseProof") {
        mUseProof = value;
        return;
    }
    if (name == "EnableDetailedOutput") {
        mEnableDetOutput = value;
        return;
    }
    if (name == "Rebuild") {
        mRebuild = value;
        return;
    }
    if (name == "ReuseAltToys") {
        mReuseAltToys = value;
        return;
    }
}

void HypoTestInvTool::SetParameter(const std::string &name,
                                   int value)
{
    // set integer parameters
    if (name == "NWorkers") {
        mNWorkers = value;
        return;
    }
    if (name == "NToyToRebuild") {
        mNToyToRebuild = value;
        return;
    }
    if (name == "RebuildParamValues") {
        mRebuildParamValues = value;
        return;
    }
    if (name == "PrintLevel") {
        mPrintLevel = value;
        return;
    }
    if (name == "InitialFit") {
        mInitialFit = value;
        return;
    }
    if (name == "RandomSeed") {
        mRandomSeed = value;
        return;
    }
    if (name == "AsimovBins") {
        mAsimovBins = value;
        return;
    }
}

void HypoTestInvTool::SetParameter(const std::string &name,
                                   double value)
{
    // set double precision parameters
    if (name == "NToysRatio") {
        mNToysRatio = value;
        return;
    }
    if (name == "MaxPOI") {
        mMaxPoi = value;
        return;
    }
}

void HypoTestInvTool::SetParameter(const std::string &name,
                                   const std::string &value)
{
    // set string parameters
    if (name == "MassValue") {
        mMassValue.assign(value);
        return;
    }
    if (name == "MinimizerType") {
        mMinimizerType.assign(value);
        return;
    }
    if (name == "ResultFolder") {
        mResultFolder = value;
        return;
    }
}

} // namespace RooStats
