#include <TFile.h>

#include "fit/HypoTestHelpers.h"
#include "fit/HypoTestUtils.h"

namespace TNLimitFinder
{

void prepare(RooWorkspace *workspace)
{
    // reset all nominal values
    resetAllValues(workspace);
    resetAllErrors(workspace);
    resetAllNominalValues(workspace);

    // activate binned likelihood
    activateBinnedLikelihood(workspace);
}

std::unique_ptr<RooStats::HypoTestInverterResult>
calculateLimit(const JobConfig &config,
               const RooStats::HypoTestInvOptions &options,
               RooWorkspace *workspace,
               bool toys,
               bool quick,
               double poimin,
               double poimax)
{
    const std::string modelSBName = "ModelConfig";
    const std::string modelBName;
    const std::string dataName = "obsData";

    int calculatorType = toys ? 0 : 2;
    bool customPoi = poimin != -1 || poimax != -1;

    auto calc = std::make_unique<RooStats::HypoTestInvTool>(options);
    auto result = std::unique_ptr<RooStats::HypoTestInverterResult>(
        calc->RunInverter(workspace,
                          modelSBName,
                          modelBName,
                          dataName,
                          calculatorType,
                          config.testStatistic(),
                          config.useCLs(),
                          quick ? config.npoints() : 20, // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
                          customPoi ? poimin : config.poimin(),
                          customPoi ? poimax : config.poimax(),
                          config.ntoys() / config.njobs()));

    if (!toys && !quick && result != nullptr) {
        std::cout << "Validating asymptotic fit" << std::endl;

        double previousMaxRange = poimax;
        size_t nScanExtensions{};
        while (true) {
            int nPointsRemoved = result->ExclusionCleanup();
            if (nPointsRemoved > 0) {
                std::cerr << "Warning: ExclusionCleanup() removed " << nPointsRemoved
                          << " scan point(s) for hypo test inversion: " << result->GetName() << std::endl;
            }

            // what's the current p-value for +2 sigma?
            // start by getting the sampling dist for the last point
            int currentNPoints = result->ArraySize();
            auto s = std::unique_ptr<RooStats::SamplingDistribution>(result->GetExpectedPValueDist(currentNPoints - 1));
            if (s == nullptr) {
                std::cerr << "Sampling distribution for the hypothesis test is empty. Exit." << std::endl;
                break;
            }

            // get its values
            const std::vector<double> &values = s->GetSamplingDistribution();

            // find indices for expected p-values
            const double nsig2 = 2.0;
            const double maxSigma = 5;
            double dsig = 2. * maxSigma / static_cast<double>(values.size() - 1);     // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            int i4 = static_cast<int>(TMath::Floor((nsig2 + maxSigma) / dsig + 0.5)); // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

            double pValueForULplus2sigma = values[i4];

            // Don't need sampling dist anymore
            s.reset();

            // Everything OK?
            if (pValueForULplus2sigma < 0.05 && result->GetExpectedUpperLimit(2) < 0.9 * result->GetXValue(currentNPoints - 1)) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
                // +2 sigma band is below 0.05 and smaller than 0.9x the current range. stop!
                std::cout << "The +2 sigma band is below 0.05 and UL+2sig is smaller than 0.9x the current range. All good!" << std::endl;
                break;
            }

            // Stop condition
            if (currentNPoints > config.npoints()) {
                std::cerr << "Extended the UL scan to more than 5x the original amount of points already (currently at " << currentNPoints << ") - won't keep going further. Pass a helpful range to configMgr instead." << std::endl;
                break;
            }

            // How much do we want to run extra in percentage?
            int nPointsDivisor = 5;             // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            if (pValueForULplus2sigma > 0.05) { // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
                std::cerr << "Warning: the +2 sigma band is not below 0.05. Will attempt to extend it." << std::endl;
            } else {
                std::cerr << "Warning: the +2 sigma UL found very close to the maximum of the scan range. Will attempt to extend it." << std::endl;
                nPointsDivisor = 10; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
            }

            // Determine range for an extra HypoTestInverter
            int nPoints = currentNPoints / nPointsDivisor;               // 20% extra points; or 10% if <0.05 but range too small
            const double oldMax = result->GetXValue(currentNPoints - 1); // mu_sig of last entry
            const double stepSize = 2 * oldMax / currentNPoints;         // twice the average step size of previous scan

            if (nPoints == 0) {
                // this happens if the current scan had precisely 1 point
                nPoints = 2;
            }

            double minRange = oldMax + stepSize; // start _beyond_ the last point
            double maxRange = oldMax + (nPoints)*stepSize;

            std::cerr << "Warning: nPoints = " << nPoints << " oldMax = " << oldMax << " stepSize = " << stepSize << std::endl;
            std::cerr << "Warning: min = " << minRange << " max = " << maxRange << std::endl;

            if (maxRange == previousMaxRange) {
                // this can happen if we e.g. extend by 3 points and all 3 points fail. In that case, we need a different range.
                ++nPoints;
                maxRange += stepSize;
            }

            std::cout << "Current scan had " << currentNPoints << " points" << std::endl;
            std::cout << "Current scan used range [" << result->GetXValue(0) << ", " << oldMax << "]" << std::endl;

            std::cerr << "Warning: running extra scan in [" << minRange << ", " << maxRange << "] with " << nPoints << " points (step size = " << stepSize << ") " << std::endl;
            sleep(1); // we want the user to be able to see what's going on here

            // Run an extra hypotest inverter
            ++nScanExtensions;
            std::cerr << "Warning: Running extension number " << nScanExtensions << " for UL scan" << std::endl;
            std::cerr << "Warning: Setting nPoints = " << nPoints << " min = " << minRange << " max = " << maxRange << std::endl;
            auto extraResult = std::unique_ptr<RooStats::HypoTestInverterResult>(
                calc->RunInverter(workspace,
                                  modelSBName,
                                  modelBName,
                                  dataName,
                                  calculatorType,
                                  config.testStatistic(),
                                  config.useCLs(),
                                  nPoints,
                                  minRange,
                                  maxRange,
                                  config.ntoys() / config.njobs()));

            if (extraResult == nullptr) {
                std::cerr << "Additional DoHypoTestInversion returned a nullptr - stopping" << std::endl;
                break;
            }

            // Append it - should re-evaluate the settings for us automatically
            std::cout << "Adding scan result to existing limit scan" << std::endl;
            if (extraResult->GetLastResult()->CLb() > 0) {
                result->Add(*extraResult);
            }
            previousMaxRange = maxRange;
        }
    }

    if (!quick && result != nullptr) {
        calc->AnalyzeResult(result.get(),
                            calculatorType);
    }

    return result;
}

void analyzeLimit(RooStats::HypoTestInverterResult *result,
                  const RooStats::HypoTestInvOptions &options,
                  bool toys)
{
    int calculatorType = toys ? 0 : 2;

    auto calc = std::make_unique<RooStats::HypoTestInvTool>(options);
    if (result != nullptr) {
        calc->AnalyzeResult(result,
                            calculatorType);
    }
}

} // namespace TNLimitFinder
