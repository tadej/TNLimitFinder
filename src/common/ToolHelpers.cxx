#include <gsl/span>

#include "common/ToolHelpers.h"

#include "Config.h"

namespace TNLimitFinder
{

std::string version()
{
    std::string v;
    v.append(Version::number);
    if (!Version::VCS.empty()) {
        v.append("+").append(Version::VCS);
    }

    return v;
}

std::string versionInfo()
{
    return "TNLimitFinder " + version();
}

std::map<std::string, docopt::value>
parseArgv(const char *usage,
          int argc,
          char **argv)
{
    const auto sargv = gsl::span<char *>(argv, argc);
    return docopt::docopt(usage,
                          {sargv.begin() + 1, sargv.end()},
                          true,
                          versionInfo());
}

} // namespace TNLimitFinder
