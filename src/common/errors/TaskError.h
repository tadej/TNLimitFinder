#ifndef TN_ERRORS_TASKERROR_
#define TN_ERRORS_TASKERROR_

#include <system_error>

namespace TNLimitFinder
{

enum class TaskError {
    // Command line parsing
    CommandLineParseError = 80, // could not parse arguments
    CommandLineRuntimeError,    // command line runtime error
};

std::error_code make_error_code(TaskError e);

} // namespace TNLimitFinder

namespace std
{

template <>
struct is_error_code_enum<TNLimitFinder::TaskError> : true_type {
};

} // namespace std

#endif // TN_ERRORS_TASKERROR_
