#include "common/Errors.h"
#include "common/errors/ConfigError.h"

namespace
{ // anonymous namespace

struct ConfigErrorCategory : std::error_category {
    [[nodiscard]] const char *name() const noexcept override;
    [[nodiscard]] std::string message(int ev) const override;
    [[nodiscard]] std::error_condition default_error_condition(int ev) const noexcept override;
};

const char *ConfigErrorCategory::name() const noexcept
{
    return "config";
}

std::string ConfigErrorCategory::message(int ev) const
{
    using TNLimitFinder::ConfigError;

    switch (static_cast<ConfigError>(ev)) {

    case ConfigError::GenericConfigurationError:
        return "Configuration implementation error";

    case ConfigError::ConfigurationLoadError:
        return "Configuration load error";
    }

    // LCOV_EXCL_START
    return "Unrecognized error";
    // LCOV_EXCL_STOP
}

std::error_condition ConfigErrorCategory::default_error_condition(int ev) const noexcept
{
    using TNLimitFinder::ConfigError;
    using TNLimitFinder::ErrorSource;

    switch (static_cast<ConfigError>(ev)) {

    case ConfigError::GenericConfigurationError:
    case ConfigError::ConfigurationLoadError:
        return ErrorSource::SystemError;
    }

    // LCOV_EXCL_START
    return {};
    // LCOV_EXCL_STOP
}

const ConfigErrorCategory theConfigErrorCategory{};

} // anonymous namespace

std::error_code TNLimitFinder::make_error_code(TNLimitFinder::ConfigError e)
{
    return {static_cast<int>(e), theConfigErrorCategory};
}
