#ifndef TN_ERRORS_INTERNALERROR_
#define TN_ERRORS_INTERNALERROR_

#include <system_error>

namespace TNLimitFinder
{

enum class InternalError {
    NotImplemented = 90,
    NotSupported,
    UndefinedBehaviour
};

std::error_code make_error_code(InternalError e);

} // namespace TNLimitFinder

namespace std
{

template <>
struct is_error_code_enum<TNLimitFinder::InternalError> : true_type {
};

} // namespace std

#endif // TN_ERRORS_INTERNALERROR_
