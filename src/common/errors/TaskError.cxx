#include "common/Errors.h"
#include "common/errors/TaskError.h"

namespace
{ // anonymous namespace

struct TaskErrorCategory : std::error_category {
    [[nodiscard]] const char *name() const noexcept override;
    [[nodiscard]] std::string message(int ev) const override;
    [[nodiscard]] std::error_condition default_error_condition(int ev) const noexcept override;
};

const char *TaskErrorCategory::name() const noexcept
{
    return "task";
}

std::string TaskErrorCategory::message(int ev) const
{
    using TNLimitFinder::TaskError;

    switch (static_cast<TaskError>(ev)) {

    case TaskError::CommandLineParseError:
        return "Command line arguments could not be parsed";

    case TaskError::CommandLineRuntimeError:
        return "Command line arguments runtime error";
    }

    // LCOV_EXCL_START
    return "Unrecognized error";
    // LCOV_EXCL_STOP
}

std::error_condition TaskErrorCategory::default_error_condition(int ev) const noexcept
{
    using TNLimitFinder::ErrorSource;
    using TNLimitFinder::TaskError;

    switch (static_cast<TaskError>(ev)) {

    case TaskError::CommandLineParseError:
        return ErrorSource::BadUserInput;

    case TaskError::CommandLineRuntimeError:
        return ErrorSource::SystemError;
    }

    // LCOV_EXCL_START
    return {};
    // LCOV_EXCL_STOP
}

const TaskErrorCategory theTaskErrorCategory{};

} // anonymous namespace

std::error_code TNLimitFinder::make_error_code(TNLimitFinder::TaskError e)
{
    return {static_cast<int>(e), theTaskErrorCategory};
}
