#include "common/Errors.h"
#include "common/errors/Exceptions.h"

namespace TNLimitFinder
{

RuntimeError::RuntimeError(const std::string &what_arg,
                           const std::error_code &error_code)
    : std::runtime_error(errorMessage(error_code, what_arg)),
      _error_code(error_code) {}

RuntimeError::RuntimeError(const char *what_arg,
                           const std::error_code &error_code)
    : std::runtime_error(errorMessage(error_code, what_arg)),
      _error_code(error_code) {}

LogicError::LogicError(const std::string &what_arg,
                       const std::error_code &error_code)
    : std::logic_error(errorMessage(error_code, what_arg)),
      _error_code(error_code) {}

LogicError::LogicError(const char *what_arg,
                       const std::error_code &error_code)
    : std::logic_error(errorMessage(error_code, what_arg)),
      _error_code(error_code) {}

} // namespace TNLimitFinder
