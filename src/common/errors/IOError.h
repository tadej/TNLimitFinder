#ifndef TN_ERRORS_IOERROR_
#define TN_ERRORS_IOERROR_

#include <system_error>

namespace TNLimitFinder
{

enum class IOError {
    // Input/output
    InputNotFound = 10,     // input file can not be opened
    OutputError,            // output file can not be opened
    InputWorkspaceNotFound, // input workspace not found
};

std::error_code make_error_code(IOError e);

} // namespace TNLimitFinder

namespace std
{

template <>
struct is_error_code_enum<TNLimitFinder::IOError> : true_type {
};

} // namespace std

#endif // TN_ERRORS_IOERROR_
