#include "common/Errors.h"
#include "common/errors/InternalError.h"

namespace
{ // anonymous namespace

struct InternalErrorCategory : std::error_category {
    [[nodiscard]] const char *name() const noexcept override;
    [[nodiscard]] std::string message(int ev) const override;
    [[nodiscard]] std::error_condition default_error_condition(int ev) const noexcept override;
};

const char *InternalErrorCategory::name() const noexcept
{
    return "internal";
}

std::string InternalErrorCategory::message(int ev) const
{
    using TNLimitFinder::InternalError;

    switch (static_cast<InternalError>(ev)) {

    case InternalError::NotImplemented:
        return "Not implemented";

    case InternalError::NotSupported:
        return "Not supported";

    case InternalError::UndefinedBehaviour:
        return "Undefined behaviour";
    }

    // LCOV_EXCL_START
    return "Unrecognized error";
    // LCOV_EXCL_STOP
}

std::error_condition InternalErrorCategory::default_error_condition(int ev) const noexcept
{
    using TNLimitFinder::ErrorSource;
    using TNLimitFinder::InternalError;

    switch (static_cast<InternalError>(ev)) {

    case InternalError::NotImplemented:
    case InternalError::NotSupported:
    case InternalError::UndefinedBehaviour:
        return ErrorSource::SystemError;
    }

    // LCOV_EXCL_START
    return {};
    // LCOV_EXCL_STOP
}

const InternalErrorCategory theInternalErrorCategory{};

} // anonymous namespace

std::error_code TNLimitFinder::make_error_code(TNLimitFinder::InternalError e)
{
    return {static_cast<int>(e), theInternalErrorCategory};
}
