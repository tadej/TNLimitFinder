#ifndef TN_ERRORS_CONFIGERROR_
#define TN_ERRORS_CONFIGERROR_

#include <system_error>

namespace TNLimitFinder
{

enum class ConfigError {
    // Configuration
    GenericConfigurationError = 20, // generic configuration error
    ConfigurationLoadError,         // could not load configuration
};

std::error_code make_error_code(ConfigError e);

} // namespace TNLimitFinder

namespace std
{

template <>
struct is_error_code_enum<TNLimitFinder::ConfigError> : true_type {
};

} // namespace std

#endif // TN_ERRORS_CONFIGERROR_
