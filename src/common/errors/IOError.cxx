#include "common/Errors.h"
#include "common/errors/IOError.h"

namespace
{ // anonymous namespace

struct IOErrorCategory : std::error_category {
    [[nodiscard]] const char *name() const noexcept override;
    [[nodiscard]] std::string message(int ev) const override;
    [[nodiscard]] std::error_condition default_error_condition(int ev) const noexcept override;
};

const char *IOErrorCategory::name() const noexcept
{
    return "io";
}

std::string IOErrorCategory::message(int ev) const
{
    using TNLimitFinder::IOError;

    switch (static_cast<IOError>(ev)) {

    case IOError::InputNotFound:
        return "Input file is missing or cannot be opened";

    case IOError::OutputError:
        return "Output file could not be written";

    case IOError::InputWorkspaceNotFound:
        return "Input workspace not found";
    }

    // LCOV_EXCL_START
    return "Unrecognized error";
    // LCOV_EXCL_STOP
}

std::error_condition IOErrorCategory::default_error_condition(int ev) const noexcept
{
    using TNLimitFinder::ErrorSource;
    using TNLimitFinder::IOError;

    switch (static_cast<IOError>(ev)) {

    case IOError::InputNotFound:
    case IOError::OutputError:
    case IOError::InputWorkspaceNotFound:
        return ErrorSource::IOError;
    }

    // LCOV_EXCL_START
    return {};
    // LCOV_EXCL_STOP
}

const IOErrorCategory theIOErrorCategory{};

} // anonymous namespace

std::error_code TNLimitFinder::make_error_code(TNLimitFinder::IOError e)
{
    return {static_cast<int>(e), theIOErrorCategory};
}
