#ifndef TN_ERRORS_EXCEPTIONS_
#define TN_ERRORS_EXCEPTIONS_

#include <stdexcept>
#include <system_error>

namespace TNLimitFinder
{

class RuntimeError : public std::runtime_error
{
public:
    explicit RuntimeError(const std::string &what_arg,
                          const std::error_code &error_code);
    explicit RuntimeError(const char *what_arg,
                          const std::error_code &error_code);

    [[nodiscard]] inline std::error_code error() const noexcept { return _error_code; }

private:
    std::error_code _error_code;
};

class LogicError : public std::logic_error
{
public:
    explicit LogicError(const std::string &what_arg,
                        const std::error_code &error_code);
    explicit LogicError(const char *what_arg,
                        const std::error_code &error_code);

    [[nodiscard]] inline std::error_code error() const noexcept { return _error_code; }

private:
    std::error_code _error_code;
};

} // namespace TNLimitFinder

#endif // TN_ERRORS_EXCEPTIONS_
