#include <iostream>
#include <sstream>

#include "common/Errors.h"

namespace
{ // anonymous namespace

class ErrorSourceCategory : public std::error_category
{
public:
    [[nodiscard]] const char *name() const noexcept override;
    [[nodiscard]] std::string message(int ev) const override;
};

// LCOV_EXCL_START
const char *ErrorSourceCategory::name() const noexcept
{
    return "source";
}
// LCOV_EXCL_STOP

std::string ErrorSourceCategory::message(int ev) const
{
    using TNLimitFinder::ErrorSource;

    switch (static_cast<ErrorSource>(ev)) {
    case ErrorSource::BadUserInput:
        return "User Input Error";

    case ErrorSource::IOError:
        return "I/O Error";

    case ErrorSource::SystemError:
        return "Internal Error";
    }

    // LCOV_EXCL_START
    return "Unrecognised Error";
    // LCOV_EXCL_STOP
}

const ErrorSourceCategory theErrorSourceCategory{};

} // anonymous namespace

std::error_condition TNLimitFinder::make_error_condition(TNLimitFinder::ErrorSource e)
{
    return {static_cast<int>(e), theErrorSourceCategory};
}

std::string TNLimitFinder::errorMessage(const std::error_code &ec,
                                     const std::string &extra)
{
    std::error_condition source = ec.default_error_condition();

    std::stringstream s;
    s << source.message() << ": " << ec
      << " \"" << ec.message() << "\"";

    if (!extra.empty()) {
        s << " \"" << extra << "\"";
    }

    return s.str();
}

int TNLimitFinder::returnError(const std::error_code &ec)
{
    std::cerr << errorMessage(ec) << std::endl;

    return ec.value();
}
