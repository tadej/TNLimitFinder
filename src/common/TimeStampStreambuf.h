#ifndef TN_TIMESTAMPSTREAMBUF_
#define TN_TIMESTAMPSTREAMBUF_

#include <streambuf>

namespace TNLimitFinder
{

class TimeStampStreambuf final : public std::streambuf
{
public:
    explicit TimeStampStreambuf(std::streambuf *dest);
    explicit TimeStampStreambuf(std::ostream &owner);
    explicit TimeStampStreambuf(const TimeStampStreambuf &buf) = delete;
    ~TimeStampStreambuf() override;

    inline TimeStampStreambuf &operator=(const TimeStampStreambuf &q) = delete;

protected:
    int overflow(int ch) final;

private:
    std::streambuf *_dest;
    std::ostream *_owner;
    bool _isAtStartOfLine;
};

} // namespace TNLimitFinder

#endif // TN_TIMESTAMPSTREAMBUF_
