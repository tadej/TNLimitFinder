#include <ctime>
#include <iostream>
#include <memory>

#include "common/TimeStampStreambuf.h"

namespace
{
constexpr int BUFFER_SIZE = 9;
}

namespace TNLimitFinder
{

TimeStampStreambuf::TimeStampStreambuf(std::streambuf *dest)
    : _dest(dest), _owner(nullptr), _isAtStartOfLine(true) {}

TimeStampStreambuf::TimeStampStreambuf(std::ostream &owner)
    : _dest(owner.rdbuf()), _owner(&owner), _isAtStartOfLine(true)
{
    _owner->rdbuf(this);
}

TimeStampStreambuf::~TimeStampStreambuf()
{
    if (_owner != nullptr) {
        _owner->rdbuf(_dest);
    }
}

int TimeStampStreambuf::overflow(int ch)
{
    // To allow truly empty lines, otherwise drop the
    // first condition...
    if (ch != '\n' && _isAtStartOfLine) {
        std::time_t t = std::time(nullptr);
        std::array<char, BUFFER_SIZE> mbptr{};
        if (std::strftime(mbptr.data(), sizeof(mbptr), "%T", std::localtime(&t)) != 0) {
            _dest->sputn("[", 1);
            _dest->sputn(mbptr.data(), sizeof(mbptr));
            _dest->sputn("] ", 2);
        }
    }
    _isAtStartOfLine = ch == '\n';
    ch = _dest->sputc(ch);

    return ch;
}

} // namespace TNLimitFinder
