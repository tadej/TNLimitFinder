#ifndef TN_ERRORS_ERRORTYPE_
#define TN_ERRORS_ERRORTYPE_

#include <system_error>

#include "common/errors/ConfigError.h"
#include "common/errors/InternalError.h"
#include "common/errors/IOError.h"
#include "common/errors/TaskError.h"

namespace TNLimitFinder
{

enum class ErrorSource {
    BadUserInput = 1, // no 0
    IOError,
    SystemError,
};

std::error_condition make_error_condition(ErrorSource e);

std::string errorMessage(const std::error_code &ec,
                         const std::string &extra = "");
int returnError(const std::error_code &ec);

} // namespace TNLimitFinder

namespace std
{

template <>
struct is_error_condition_enum<TNLimitFinder::ErrorSource> : true_type {
};

} // namespace std

#endif // TN_ERRORS_ERRORTYPE_
