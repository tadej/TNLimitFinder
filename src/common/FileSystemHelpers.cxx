#include <iostream>

#include "common/Errors.h"
#include "common/FileSystemHelpers.h"

namespace TNLimitFinder::FS
{

std::unique_ptr<TFile> openFile(const std::filesystem::path &path,
                                int *status)
{
    if (status != nullptr && *status != 0) {
        return nullptr;
    }

    auto file = std::unique_ptr<TFile>(TFile::Open(path.c_str()));
    if (!file || !file->IsOpen()) {
        std::cerr << "Problems opening " << path << std::endl;
        if (status != nullptr) {
            *status = returnError(IOError::InputNotFound);
        }
        return nullptr;
    }

    return file;
}

std::unique_ptr<TFile> createFile(const std::filesystem::path &path,
                                  bool update,
                                  int *status)
{
    if (status != nullptr && *status != 0) {
        return nullptr;
    }

    auto file = std::make_unique<TFile>(path.c_str(), update ? "update" : "recreate");
    if (!file || !file->IsOpen()) {
        std::cerr << "Problems writing " << path << std::endl;
        if (status != nullptr) {
            *status = returnError(IOError::OutputError);
        }
        return nullptr;
    }

    return file;
}

std::vector<std::filesystem::path> findFiles(const std::string &baseName,
                                             const std::string &inputFolder)
{
    std::vector<std::filesystem::path> files;
    for (const std::filesystem::directory_entry &p : std::filesystem::directory_iterator(inputFolder)) {
        if (p.path().filename().string().find(baseName + ".") != 0) {
            continue;
        }

        files.emplace_back(p.path());
    }

    return files;
}

} // namespace TNLimitFinder::FS
