#ifndef TN_TOOLHELPERS_
#define TN_TOOLHELPERS_

#include <string>

#include <docopt/docopt.h>

namespace TNLimitFinder
{

std::string version();
std::string versionInfo();

std::map<std::string, docopt::value> parseArgv(const char *usage,
                                               int argc,
                                               char **argv);

} // namespace TNLimitFinder

#endif // TN_TOOLHELPERS_
