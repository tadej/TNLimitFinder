#ifndef TN_FILESYSTEMHELPERS_
#define TN_FILESYSTEMHELPERS_

#include <filesystem>
#include <string>

#include <TFile.h>

namespace TNLimitFinder
{

namespace FS
{

std::unique_ptr<TFile> openFile(const std::filesystem::path &path,
                                int *status);
std::unique_ptr<TFile> createFile(const std::filesystem::path &path,
                                  bool update,
                                  int *status);
std::vector<std::filesystem::path> findFiles(const std::string &baseName,
                                             const std::string &inputFolder);

} // namespace FS

} // namespace TNLimitFinder

#endif // TN_FILESYSTEMHELPERS_
