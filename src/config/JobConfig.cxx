#include <fstream>
#include <iostream>

#include <TROOT.h>

#include "common/errors/ConfigError.h"
#include "common/errors/Exceptions.h"
#include "config/JobConfig.h"

namespace TNLimitFinder
{

JobConfig::JobConfig(const std::string &file)
{
    // Common global config
    gROOT->SetBatch(true);
    TDirectory::AddDirectory(false);

    // Read a the JSON config file
    std::ifstream i(file);
    json job;
    i >> job;

    if (!job["test_stat"].is_null()) {
        _testStatistic = job["test_stat"].get<int>();
    }

    if (!job["useCLs"].is_null()) {
        _useCLs = job["useCLs"].get<bool>();
    }

    if (!job["npoints"].is_null()) {
        _npoints = job["npoints"].get<int>();
    }

    if (!job["poimin"].is_null()) {
        _poimin = job["poimin"].get<int>();
    }
    if (!job["poimax"].is_null()) {
        _poimax = job["poimax"].get<int>();
    }

    if (!job["ntoys"].is_null()) {
        _ntoys = job["ntoys"].get<int>();
    }
    if (!job["njobs"].is_null()) {
        _njobs = job["njobs"].get<int>();
    }
}

} // namespace TNLimitFinder
