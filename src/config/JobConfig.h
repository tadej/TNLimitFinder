#ifndef TN_JOBCONFIG_
#define TN_JOBCONFIG_

#include <vector>

#include <libs/json.hpp>
using json = nlohmann::json;

namespace TNLimitFinder
{

class JobConfig
{
public:
    explicit JobConfig(const std::string &file);

    [[nodiscard]] inline int testStatistic() const { return _testStatistic; }
    [[nodiscard]] inline bool useCLs() const { return _useCLs; }
    [[nodiscard]] inline int npoints() const { return _npoints; }
    [[nodiscard]] inline int poimin() const { return _poimin; }
    [[nodiscard]] inline int poimax() const { return _poimax; }
    [[nodiscard]] inline int ntoys() const { return _ntoys; }
    [[nodiscard]] inline int njobs() const { return _njobs; }

private:
    int _testStatistic{3};
    bool _useCLs{true};
    int _npoints{25}; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    int _poimin{0};
    int _poimax{50};   // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    int _ntoys{10000}; // NOLINT(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
    int _njobs{1};
};

} // namespace TNLimitFinder

#endif // TN_JOBCONFIG_
